 %% computes aggregated and yearly error Metrics for Arroyo (basin 0) and for Alameda (basin 1)
function [errTable, yearlyTable, monthlyTable] = errorMetrics(trainTable,testTable, basinNumber, runName)
% trainTable = table(trainDateTimes, trainY, trainedY, PRMSuncaltrain, PRMScalibtrain, SACSMAuncaltrain, SACSMAcalibtrain, IHACRESuncaltrain, IHACREScalibtrain, Raintrain, ...
%     'VariableNames',{'DateTime','Observed',runName,'PRMS_uncal','PRMS_calib','SACSMA_uncal','SACSMA_calib','IHACRES_uncal','IHACRES_calib','Obs_Precip'});
% testTable = table(testDateTimes, testY, predictedY, PRMSuncaltest, PRMScalibtest, SACSMAuncaltest, SACSMAcalibtest, IHACRESuncaltest, IHACREScalibtest, Raintest, ...
%     'VariableNames',{'DateTime','Observed',runName,'PRMS_uncal','PRMS_calib','SACSMA_uncal','SACSMA_calib','IHACRES_uncal','IHACRES_calib','Obs_Precip'});

    % set basin area in m^2 depending on which basin is selected
    % Here are the basin areas (in square feet):
    % Arroyo Hondo: 2135840088.53 ft^2
    % Upper Alameda: 932039752.807 ft^2

    if (basinNumber == 0)
        area = 2135840088.53 / 10.764; 
    elseif (basinNumber == 1)
        area = 932039752.807 / 10.764; 
    end
    
    % set start and end of water years
    begWY = 1997;
    endWY = 2020;
    numWY = endWY-begWY+1;
    begTrainWY = 1997;
    endTrainWY = 2014;
    numTrainWY = endTrainWY-begTrainWY+1;
    begTestWY = 2015;
    endTestWY = 2020;
    numTestWY = endTestWY-begTestWY+1;
    
    % unpack tables
    VariableNames = trainTable.Properties.VariableNames; % variable names
    trainDates = table2array(trainTable(:,1));
    trainObs = table2array(trainTable(:,2));
    trainLSTM = table2array(trainTable(:,3));
    trainPRMSuncal = table2array(trainTable(:,4));
    trainPRMScalib = table2array(trainTable(:,5));
    trainSACSMAuncal = table2array(trainTable(:,6));
    trainSACSMAcalib = table2array(trainTable(:,7));
    trainIHACRESuncal = table2array(trainTable(:,8));
    trainIHACREScalib = table2array(trainTable(:,9));
    trainRain = table2array(trainTable(:,10));

    testDates = table2array(testTable(:,1));
    testObs = table2array(testTable(:,2));
    testLSTM = table2array(testTable(:,3));
    testPRMSuncal = table2array(testTable(:,4));
    testPRMScalib = table2array(testTable(:,5));
    testSACSMAuncal = table2array(testTable(:,6));
    testSACSMAcalib = table2array(testTable(:,7));
    testIHACRESuncal = table2array(testTable(:,8));
    testIHACREScalib = table2array(testTable(:,9));
    testRain = table2array(testTable(:,10));
    
    trainWYears = getWY(trainDates);
    testWYears = getWY(testDates);
    allDates = [trainDates;testDates];
    allObs = [trainObs;testObs];
    allLSTM = [trainLSTM;testLSTM];
    allPRMSuncal = [trainPRMSuncal;testPRMSuncal];
    allPRMScalib = [trainPRMScalib;testPRMScalib];
    allSACSMAuncal = [trainSACSMAuncal;testSACSMAuncal];
    allSACSMAcalib = [trainSACSMAcalib;testSACSMAcalib];
    allIHACRESuncal = [trainIHACRESuncal;testIHACRESuncal];
    allIHACREScalib = [trainIHACREScalib;testIHACREScalib];
    allRain = [trainRain;testRain];
    allWYears = [trainWYears;testWYears];

    %% yearly discharge volume totals
    % allocate train containers
    trainObsVol = zeros(numTrainWY, 1);
    trainLSTMVol = zeros(numTrainWY, 1);
    trainPRMSuncalVol = zeros(numTrainWY, 1);
    trainPRMScalibVol = zeros(numTrainWY, 1);
    trainSACSMAuncalVol = zeros(numTrainWY, 1);
    trainSACSMAcalibVol = zeros(numTrainWY, 1);
    trainIHACRESuncalVol = zeros(numTrainWY, 1);
    trainIHACREScalibVol = zeros(numTrainWY, 1);
    
    % seconds per day
    secsperday = 60*60*24;
    
    % loop over train water years
    n = 0;
    for myYear = begTrainWY:endTrainWY
        n = n+1;
        
        % get year of flows
        indWY = (allWYears == myYear);
        myObs = allObs(indWY);
        myLSTM = allLSTM(indWY);
        myPRMSuncal = allPRMSuncal(indWY);
        myPRMScalib = allPRMScalib(indWY);
        mySACSMAuncal = allSACSMAuncal(indWY);
        mySACSMAcalib = allSACSMAcalib(indWY);
        myIHACRESuncal = allIHACRESuncal(indWY);
        myIHACREScalib = allIHACREScalib(indWY);
        
        % compute volume
        trainObsVol(n) = sum(myObs.*secsperday);
        trainLSTMVol(n) = sum(myLSTM.*secsperday);
        trainPRMSuncalVol(n) = sum(myPRMSuncal.*secsperday);
        trainPRMScalibVol(n) = sum(myPRMScalib.*secsperday);
        trainSACSMAuncalVol(n) = sum(mySACSMAuncal.*secsperday);
        trainSACSMAcalibVol(n) = sum(mySACSMAcalib.*secsperday);
        trainIHACRESuncalVol(n) = sum(myIHACRESuncal.*secsperday);
        trainIHACREScalibVol(n) = sum(myIHACREScalib.*secsperday);
    end
    
    % allocate test containers
    testObsVol = zeros(numTestWY, 1);
    testLSTMVol = zeros(numTestWY, 1);
    testPRMSuncalVol = zeros(numTestWY, 1);
    testPRMScalibVol = zeros(numTestWY, 1);
    testSACSMAuncalVol = zeros(numTestWY, 1);
    testSACSMAcalibVol = zeros(numTestWY, 1);
    testIHACRESuncalVol = zeros(numTestWY, 1);
    testIHACREScalibVol = zeros(numTestWY, 1);
    
    % loop over test water years
    n = 0;
    for myYear = begTestWY:endTestWY
        n = n+1;
        
        % get year of flows
        indWY = (allWYears == myYear);
        myObs = allObs(indWY);
        myLSTM = allLSTM(indWY);
        myPRMSuncal = allPRMSuncal(indWY);
        myPRMScalib = allPRMScalib(indWY);
        mySACSMAuncal = allSACSMAuncal(indWY);
        mySACSMAcalib = allSACSMAcalib(indWY);
        myIHACRESuncal = allIHACRESuncal(indWY);
        myIHACREScalib = allIHACREScalib(indWY);
        
        % compute volume
        testObsVol(n) = sum(myObs.*secsperday);
        testLSTMVol(n) = sum(myLSTM.*secsperday);
        testPRMSuncalVol(n) = sum(myPRMSuncal.*secsperday);
        testPRMScalibVol(n) = sum(myPRMScalib.*secsperday);
        testSACSMAuncalVol(n) = sum(mySACSMAuncal.*secsperday);
        testSACSMAcalibVol(n) = sum(mySACSMAcalib.*secsperday);
        testIHACRESuncalVol(n) = sum(myIHACRESuncal.*secsperday);
        testIHACREScalibVol(n) = sum(myIHACREScalib.*secsperday);
    end
    
    %% aggregated error metrics

    % LSTM
    [trainLSTM_RMSE, trainLSTM_NRMSE] = RMSE(trainLSTM, trainObs);
    [testLSTM_RMSE, testLSTM_NRMSE] = RMSE(testLSTM, testObs);
    trainLSTM_R2 = RMSE(trainLSTM, trainObs);
    testLSTM_R2 = RMSE(testLSTM, testObs);
    [trainLSTM_NSE, trainLSTM_aNSE, trainLSTM_bNSE, trainLSTM_r] = NSE(trainLSTM, trainObs);
    [testLSTM_NSE, testLSTM_aNSE, testLSTM_bNSE, testLSTM_r] = NSE(testLSTM, testObs);
    [trainLSTM_NSEH, trainLSTM_aNSEH, trainLSTM_bNSEH, trainLSTM_rH] = NSEH(trainLSTM, trainObs);
    [testLSTM_NSEH, testLSTM_aNSEH, testLSTM_bNSEH, testLSTM_rH] = NSEH(testLSTM, testObs);
    [trainLSTM_NSEL, trainLSTM_aNSEL, trainLSTM_bNSEL, trainLSTM_rL] = NSEL(trainLSTM, trainObs);
    [testLSTM_NSEL, testLSTM_aNSEL, testLSTM_bNSEL, testLSTM_rL] = NSEL(testLSTM, testObs);
    [trainLSTM_KGE, trainLSTM_aKGE, trainLSTM_bKGE] = KGE(trainLSTM, trainObs);
    [testLSTM_KGE, testLSTM_aKGE, testLSTM_bKGE] = KGE(testLSTM, testObs);
    [trainLSTM_KGEL, trainLSTM_aKGEL, trainLSTM_bKGEL] = KGEL(trainLSTM, trainObs);
    [testLSTM_KGEL, testLSTM_aKGEL, testLSTM_bKGEL] = KGEL(testLSTM, testObs);
    [trainLSTM_KGEH, trainLSTM_aKGEH, trainLSTM_bKGEH] = KGEH(trainLSTM, trainObs);
    [testLSTM_KGEH, testLSTM_aKGEH, testLSTM_bKGEH] = KGEH(testLSTM, testObs);
    trainLSTM_FHV = FHV(trainLSTM, trainObs);
    testLSTM_FHV = FHV(testLSTM, testObs);
    trainLSTM_FLV = FLV(trainLSTM, trainObs);
    testLSTM_FLV = FLV(testLSTM, testObs);
    trainLSTM_FMS = FMS(trainLSTM, trainObs);
    testLSTM_FMS = FMS(testLSTM, testObs);
    [trainLSTM_FMR, trainLSTM_FMAR] = FMR(trainLSTM, trainObs);
    [testLSTM_FMR, testLSTM_FMAR] = FMR(testLSTM, testObs);
    trainLSTM_MI = MI(trainLSTM, trainObs);
    testLSTM_MI = MI(testLSTM, testObs);
    [trainLSTM_RR, trainLSTM_WRR, trainLSTM_DRR] = RR(trainLSTM, trainObs, trainRain, trainDates, area);
    [testLSTM_RR, testLSTM_WRR, testLSTM_DRR] = RR(testLSTM, testObs, testRain, testDates, area);
    trainLSTM_YVNSE = NSE(trainLSTMVol, trainObsVol);
    testLSTM_YVNSE = NSE(testLSTMVol, testObsVol);
    trainLSTM_YVRMSE = RMSE(trainLSTMVol, trainObsVol);
    testLSTM_YVRMSE = RMSE(testLSTMVol, testObsVol);
    trainLSTM_YVKGE = KGE(trainLSTMVol, trainObsVol);
    testLSTM_YVKGE = KGE(testLSTMVol, testObsVol);

    % added new metric
    [trainLSTM_SQRTNSE, trainLSTM_aSQRTNSE, trainLSTM_bSQRTNSE, trainLSTM_SQRTr] = NSE(sqrt(trainLSTM), sqrt(trainObs));
    [testLSTM_SQRTNSE, testLSTM_aSQRTNSE, testLSTM_bSQRTNSE, testLSTM_SQRTr] = NSE(sqrt(testLSTM), sqrt(testObs));

    % uncalibrated PRMS
    [trainPRMSuncal_RMSE, trainPRMSuncal_NRMSE] = RMSE(trainPRMSuncal, trainObs);
    [testPRMSuncal_RMSE, testPRMSuncal_NRMSE] = RMSE(testPRMSuncal, testObs);
    trainPRMSuncal_R2 = RMSE(trainPRMSuncal, trainObs);
    testPRMSuncal_R2 = RMSE(testPRMSuncal, testObs);
    [trainPRMSuncal_NSE, trainPRMSuncal_aNSE, trainPRMSuncal_bNSE, trainPRMSuncal_r] = NSE(trainPRMSuncal, trainObs);
    [testPRMSuncal_NSE, testPRMSuncal_aNSE, testPRMSuncal_bNSE, testPRMSuncal_r] = NSE(testPRMSuncal, testObs);
    [trainPRMSuncal_NSEH, trainPRMSuncal_aNSEH, trainPRMSuncal_bNSEH, trainPRMSuncal_rH] = NSEH(trainPRMSuncal, trainObs);
    [testPRMSuncal_NSEH, testPRMSuncal_aNSEH, testPRMSuncal_bNSEH, testPRMSuncal_rH] = NSEH(testPRMSuncal, testObs);
    [trainPRMSuncal_NSEL, trainPRMSuncal_aNSEL, trainPRMSuncal_bNSEL, trainPRMSuncal_rL] = NSEL(trainPRMSuncal, trainObs);
    [testPRMSuncal_NSEL, testPRMSuncal_aNSEL, testPRMSuncal_bNSEL, testPRMSuncal_rL] = NSEL(testPRMSuncal, testObs);
    [trainPRMSuncal_KGE, trainPRMSuncal_aKGE, trainPRMSuncal_bKGE] = KGE(trainPRMSuncal, trainObs);
    [testPRMSuncal_KGE, testPRMSuncal_aKGE, testPRMSuncal_bKGE] = KGE(testPRMSuncal, testObs);
    [trainPRMSuncal_KGEL, trainPRMSuncal_aKGEL, trainPRMSuncal_bKGEL] = KGEL(trainPRMSuncal, trainObs);
    [testPRMSuncal_KGEL, testPRMSuncal_aKGEL, testPRMSuncal_bKGEL] = KGEL(testPRMSuncal, testObs);
    [trainPRMSuncal_KGEH, trainPRMSuncal_aKGEH, trainPRMSuncal_bKGEH] = KGEH(trainPRMSuncal, trainObs);
    [testPRMSuncal_KGEH, testPRMSuncal_aKGEH, testPRMSuncal_bKGEH] = KGEH(testPRMSuncal, testObs);
    trainPRMSuncal_FHV = FHV(trainPRMSuncal, trainObs);
    testPRMSuncal_FHV = FHV(testPRMSuncal, testObs);
    trainPRMSuncal_FLV = FLV(trainPRMSuncal, trainObs);
    testPRMSuncal_FLV = FLV(testPRMSuncal, testObs);
    trainPRMSuncal_FMS = FMS(trainPRMSuncal, trainObs);
    testPRMSuncal_FMS = FMS(testPRMSuncal, testObs);
    [trainPRMSuncal_FMR, trainPRMSuncal_FMAR] = FMR(trainPRMSuncal, trainObs);
    [testPRMSuncal_FMR, testPRMSuncal_FMAR] = FMR(testPRMSuncal, testObs);
    trainPRMSuncal_MI = MI(trainPRMSuncal, trainObs);
    testPRMSuncal_MI = MI(testPRMSuncal, testObs);
    [trainPRMSuncal_RR, trainPRMSuncal_WRR, trainPRMSuncal_DRR] = RR(trainPRMSuncal, trainObs, trainRain, trainDates, area);
    [testPRMSuncal_RR, testPRMSuncal_WRR, testPRMSuncal_DRR] = RR(testPRMSuncal, testObs, testRain, testDates, area);
    trainPRMSuncal_YVNSE = NSE(trainPRMSuncalVol, trainObsVol);
    testPRMSuncal_YVNSE = NSE(testPRMSuncalVol, testObsVol);
    trainPRMSuncal_YVRMSE = RMSE(trainPRMSuncalVol, trainObsVol);
    testPRMSuncal_YVRMSE = RMSE(testPRMSuncalVol, testObsVol);
    trainPRMSuncal_YVKGE = KGE(trainPRMSuncalVol, trainObsVol);
    testPRMSuncal_YVKGE = KGE(testPRMSuncalVol, testObsVol);

    % added new metric
    [trainPRMSuncal_SQRTNSE, trainPRMSuncal_aQSRTNSE, trainPRMSuncal_bSQRTNSE, trainPRMSuncal_SQRTr] = NSE(sqrt(trainPRMSuncal), sqrt(trainObs));
    [testPRMSuncal_SQRTNSE, testPRMSuncal_aSQRTNSE, testPRMSuncal_bSQRTNSE, testPRMSuncal_SQRTr] = NSE(sqrt(testPRMSuncal), sqrt(testObs));

    % calibrated PRMS
    [trainPRMScalib_RMSE, trainPRMScalib_NRMSE] = RMSE(trainPRMScalib, trainObs);
    [testPRMScalib_RMSE, testPRMScalib_NRMSE] = RMSE(testPRMScalib, testObs);
    trainPRMScalib_R2 = RMSE(trainPRMScalib, trainObs);
    testPRMScalib_R2 = RMSE(testPRMScalib, testObs);
    [trainPRMScalib_NSE, trainPRMScalib_aNSE, trainPRMScalib_bNSE, trainPRMScalib_r] = NSE(trainPRMScalib, trainObs);
    [testPRMScalib_NSE, testPRMScalib_aNSE, testPRMScalib_bNSE, testPRMScalib_r] = NSE(testPRMScalib, testObs);
    [trainPRMScalib_NSEH, trainPRMScalib_aNSEH, trainPRMScalib_bNSEH, trainPRMScalib_rH] = NSEH(trainPRMScalib, trainObs);
    [testPRMScalib_NSEH, testPRMScalib_aNSEH, testPRMScalib_bNSEH, testPRMScalib_rH] = NSEH(testPRMScalib, testObs);
    [trainPRMScalib_NSEL, trainPRMScalib_aNSEL, trainPRMScalib_bNSEL, trainPRMScalib_rL] = NSEL(trainPRMScalib, trainObs);
    [testPRMScalib_NSEL, testPRMScalib_aNSEL, testPRMScalib_bNSEL, testPRMScalib_rL] = NSEL(testPRMScalib, testObs);
    [trainPRMScalib_KGE, trainPRMScalib_aKGE, trainPRMScalib_bKGE] = KGE(trainPRMScalib, trainObs);
    [testPRMScalib_KGE, testPRMScalib_aKGE, testPRMScalib_bKGE] = KGE(testPRMScalib, testObs);
    [trainPRMScalib_KGEL, trainPRMScalib_aKGEL, trainPRMScalib_bKGEL] = KGEL(trainPRMScalib, trainObs);
    [testPRMScalib_KGEL, testPRMScalib_aKGEL, testPRMScalib_bKGEL] = KGEL(testPRMScalib, testObs);
    [trainPRMScalib_KGEH, trainPRMScalib_aKGEH, trainPRMScalib_bKGEH] = KGEH(trainPRMScalib, trainObs);
    [testPRMScalib_KGEH, testPRMScalib_aKGEH, testPRMScalib_bKGEH] = KGEH(testPRMScalib, testObs);
    trainPRMScalib_FHV = FHV(trainPRMScalib, trainObs);
    testPRMScalib_FHV = FHV(testPRMScalib, testObs);
    trainPRMScalib_FLV = FLV(trainPRMScalib, trainObs);
    testPRMScalib_FLV = FLV(testPRMScalib, testObs);
    trainPRMScalib_FMS = FMS(trainPRMScalib, trainObs);
    testPRMScalib_FMS = FMS(testPRMScalib, testObs);
    [trainPRMScalib_FMR, trainPRMScalib_FMAR] = FMR(trainPRMScalib, trainObs);
    [testPRMScalib_FMR, testPRMScalib_FMAR] = FMR(testPRMScalib, testObs);
    trainPRMScalib_MI = MI(trainPRMScalib, trainObs);
    testPRMScalib_MI = MI(testPRMScalib, testObs);
    [trainPRMScalib_RR, trainPRMScalib_WRR, trainPRMScalib_DRR] = RR(trainPRMScalib, trainObs, trainRain, trainDates, area);
    [testPRMScalib_RR, testPRMScalib_WRR, testPRMScalib_DRR] = RR(testPRMScalib, testObs, testRain, testDates, area);
    trainPRMScalib_YVNSE = NSE(trainPRMScalibVol, trainObsVol);
    testPRMScalib_YVNSE = NSE(testPRMScalibVol, testObsVol);
    trainPRMScalib_YVRMSE = RMSE(trainPRMScalibVol, trainObsVol);
    testPRMScalib_YVRMSE = RMSE(testPRMScalibVol, testObsVol);
    trainPRMScalib_YVKGE = KGE(trainPRMScalibVol, trainObsVol);
    testPRMScalib_YVKGE = KGE(testPRMScalibVol, testObsVol);
    
    % added new metric
    [trainPRMScalib_SQRTNSE, trainPRMScalib_aQSRTNSE, trainPRMScalib_bSQRTNSE, trainPRMScalib_SQRTr] = NSE(sqrt(trainPRMScalib), sqrt(trainObs));
    [testPRMScalib_SQRTNSE, testPRMScalib_aSQRTNSE, testPRMScalib_bSQRTNSE, testPRMScalib_SQRTr] = NSE(sqrt(testPRMScalib), sqrt(testObs));

    % uncalibrated SACSMA
    [trainSACSMAuncal_RMSE, trainSACSMAuncal_NRMSE] = RMSE(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_RMSE, testSACSMAuncal_NRMSE] = RMSE(testSACSMAuncal, testObs);
    trainSACSMAuncal_R2 = RMSE(trainSACSMAuncal, trainObs);
    testSACSMAuncal_R2 = RMSE(testSACSMAuncal, testObs);
    [trainSACSMAuncal_NSE, trainSACSMAuncal_aNSE, trainSACSMAuncal_bNSE, trainSACSMAuncal_r] = NSE(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_NSE, testSACSMAuncal_aNSE, testSACSMAuncal_bNSE, testSACSMAuncal_r] = NSE(testSACSMAuncal, testObs);
    [trainSACSMAuncal_NSEH, trainSACSMAuncal_aNSEH, trainSACSMAuncal_bNSEH, trainSACSMAuncal_rH] = NSEH(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_NSEH, testSACSMAuncal_aNSEH, testSACSMAuncal_bNSEH, testSACSMAuncal_rH] = NSEH(testSACSMAuncal, testObs);
    [trainSACSMAuncal_NSEL, trainSACSMAuncal_aNSEL, trainSACSMAuncal_bNSEL, trainSACSMAuncal_rL] = NSEL(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_NSEL, testSACSMAuncal_aNSEL, testSACSMAuncal_bNSEL, testSACSMAuncal_rL] = NSEL(testSACSMAuncal, testObs);
    [trainSACSMAuncal_KGE, trainSACSMAuncal_aKGE, trainSACSMAuncal_bKGE] = KGE(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_KGE, testSACSMAuncal_aKGE, testSACSMAuncal_bKGE] = KGE(testSACSMAuncal, testObs);
    [trainSACSMAuncal_KGEL, trainSACSMAuncal_aKGEL, trainSACSMAuncal_bKGEL] = KGEL(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_KGEL, testSACSMAuncal_aKGEL, testSACSMAuncal_bKGEL] = KGEL(testSACSMAuncal, testObs);
    [trainSACSMAuncal_KGEH, trainSACSMAuncal_aKGEH, trainSACSMAuncal_bKGEH] = KGEH(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_KGEH, testSACSMAuncal_aKGEH, testSACSMAuncal_bKGEH] = KGEH(testSACSMAuncal, testObs);
    trainSACSMAuncal_FHV = FHV(trainSACSMAuncal, trainObs);
    testSACSMAuncal_FHV = FHV(testSACSMAuncal, testObs);
    trainSACSMAuncal_FLV = FLV(trainSACSMAuncal, trainObs);
    testSACSMAuncal_FLV = FLV(testSACSMAuncal, testObs);
    trainSACSMAuncal_FMS = FMS(trainSACSMAuncal, trainObs);
    testSACSMAuncal_FMS = FMS(testSACSMAuncal, testObs);
    [trainSACSMAuncal_FMR, trainSACSMAuncal_FMAR] = FMR(trainSACSMAuncal, trainObs);
    [testSACSMAuncal_FMR, testSACSMAuncal_FMAR] = FMR(testSACSMAuncal, testObs);
    trainSACSMAuncal_MI = MI(trainSACSMAuncal, trainObs);
    testSACSMAuncal_MI = MI(testSACSMAuncal, testObs);
    [trainSACSMAuncal_RR, trainSACSMAuncal_WRR, trainSACSMAuncal_DRR] = RR(trainSACSMAuncal, trainObs, trainRain, trainDates, area);
    [testSACSMAuncal_RR, testSACSMAuncal_WRR, testSACSMAuncal_DRR] = RR(testSACSMAuncal, testObs, testRain, testDates, area);
    trainSACSMAuncal_YVNSE = NSE(trainSACSMAuncalVol, trainObsVol);
    testSACSMAuncal_YVNSE = NSE(testSACSMAuncalVol, testObsVol);
    trainSACSMAuncal_YVRMSE = RMSE(trainSACSMAuncalVol, trainObsVol);
    testSACSMAuncal_YVRMSE = RMSE(testSACSMAuncalVol, testObsVol);
    trainSACSMAuncal_YVKGE = KGE(trainSACSMAuncalVol, trainObsVol);
    testSACSMAuncal_YVKGE = KGE(testSACSMAuncalVol, testObsVol);

    % added new metric
    [trainSACSMAuncal_SQRTNSE, trainSACSMAuncal_aQSRTNSE, trainSACSMAuncal_bSQRTNSE, trainSACSMAuncal_SQRTr] = NSE(sqrt(trainSACSMAuncal), sqrt(trainObs));
    [testSACSMAuncal_SQRTNSE, testSACSMAuncal_aSQRTNSE, testSACSMAuncal_bSQRTNSE, testSACSMAuncal_SQRTr] = NSE(sqrt(testSACSMAuncal), sqrt(testObs));

    % calibrated SACSMA
    [trainSACSMAcalib_RMSE, trainSACSMAcalib_NRMSE] = RMSE(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_RMSE, testSACSMAcalib_NRMSE] = RMSE(testSACSMAcalib, testObs);
    trainSACSMAcalib_R2 = RMSE(trainSACSMAcalib, trainObs);
    testSACSMAcalib_R2 = RMSE(testSACSMAcalib, testObs);
    [trainSACSMAcalib_NSE, trainSACSMAcalib_aNSE, trainSACSMAcalib_bNSE, trainSACSMAcalib_r] = NSE(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_NSE, testSACSMAcalib_aNSE, testSACSMAcalib_bNSE, testSACSMAcalib_r] = NSE(testSACSMAcalib, testObs);
    [trainSACSMAcalib_NSEH, trainSACSMAcalib_aNSEH, trainSACSMAcalib_bNSEH, trainSACSMAcalib_rH] = NSEH(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_NSEH, testSACSMAcalib_aNSEH, testSACSMAcalib_bNSEH, testSACSMAcalib_rH] = NSEH(testSACSMAcalib, testObs);
    [trainSACSMAcalib_NSEL, trainSACSMAcalib_aNSEL, trainSACSMAcalib_bNSEL, trainSACSMAcalib_rL] = NSEL(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_NSEL, testSACSMAcalib_aNSEL, testSACSMAcalib_bNSEL, testSACSMAcalib_rL] = NSEL(testSACSMAcalib, testObs);
    [trainSACSMAcalib_KGE, trainSACSMAcalib_aKGE, trainSACSMAcalib_bKGE] = KGE(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_KGE, testSACSMAcalib_aKGE, testSACSMAcalib_bKGE] = KGE(testSACSMAcalib, testObs);
    [trainSACSMAcalib_KGEL, trainSACSMAcalib_aKGEL, trainSACSMAcalib_bKGEL] = KGEL(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_KGEL, testSACSMAcalib_aKGEL, testSACSMAcalib_bKGEL] = KGEL(testSACSMAcalib, testObs);
    [trainSACSMAcalib_KGEH, trainSACSMAcalib_aKGEH, trainSACSMAcalib_bKGEH] = KGEH(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_KGEH, testSACSMAcalib_aKGEH, testSACSMAcalib_bKGEH] = KGEH(testSACSMAcalib, testObs);
    trainSACSMAcalib_FHV = FHV(trainSACSMAcalib, trainObs);
    testSACSMAcalib_FHV = FHV(testSACSMAcalib, testObs);
    trainSACSMAcalib_FLV = FLV(trainSACSMAcalib, trainObs);
    testSACSMAcalib_FLV = FLV(testSACSMAcalib, testObs);
    trainSACSMAcalib_FMS = FMS(trainSACSMAcalib, trainObs);
    testSACSMAcalib_FMS = FMS(testSACSMAcalib, testObs);
    [trainSACSMAcalib_FMR, trainSACSMAcalib_FMAR] = FMR(trainSACSMAcalib, trainObs);
    [testSACSMAcalib_FMR, testSACSMAcalib_FMAR] = FMR(testSACSMAcalib, testObs);
    trainSACSMAcalib_MI = MI(trainSACSMAcalib, trainObs);
    testSACSMAcalib_MI = MI(testSACSMAcalib, testObs);
    [trainSACSMAcalib_RR, trainSACSMAcalib_WRR, trainSACSMAcalib_DRR] = RR(trainSACSMAcalib, trainObs, trainRain, trainDates, area);
    [testSACSMAcalib_RR, testSACSMAcalib_WRR, testSACSMAcalib_DRR] = RR(testSACSMAcalib, testObs, testRain, testDates, area);
    trainSACSMAcalib_YVNSE = NSE(trainSACSMAcalibVol, trainObsVol);
    testSACSMAcalib_YVNSE = NSE(testSACSMAcalibVol, testObsVol);
    trainSACSMAcalib_YVRMSE = RMSE(trainSACSMAcalibVol, trainObsVol);
    testSACSMAcalib_YVRMSE = RMSE(testSACSMAcalibVol, testObsVol);
    trainSACSMAcalib_YVKGE = KGE(trainSACSMAcalibVol, trainObsVol);
    testSACSMAcalib_YVKGE = KGE(testSACSMAcalibVol, testObsVol);
    
    % added new metric
    [trainSACSMAcalib_SQRTNSE, trainSACSMAcalib_aQSRTNSE, trainSACSMAcalib_bSQRTNSE, trainSACSMAcalib_SQRTr] = NSE(sqrt(trainSACSMAcalib), sqrt(trainObs));
    [testSACSMAcalib_SQRTNSE, testSACSMAcalib_aSQRTNSE, testSACSMAcalib_bSQRTNSE, testSACSMAcalib_SQRTr] = NSE(sqrt(testSACSMAcalib), sqrt(testObs));

    % uncalibrated IHACRES
    [trainIHACRESuncal_RMSE, trainIHACRESuncal_NRMSE] = RMSE(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_RMSE, testIHACRESuncal_NRMSE] = RMSE(testIHACRESuncal, testObs);
    trainIHACRESuncal_R2 = RMSE(trainIHACRESuncal, trainObs);
    testIHACRESuncal_R2 = RMSE(testIHACRESuncal, testObs);
    [trainIHACRESuncal_NSE, trainIHACRESuncal_aNSE, trainIHACRESuncal_bNSE, trainIHACRESuncal_r] = NSE(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_NSE, testIHACRESuncal_aNSE, testIHACRESuncal_bNSE, testIHACRESuncal_r] = NSE(testIHACRESuncal, testObs);
    [trainIHACRESuncal_NSEH, trainIHACRESuncal_aNSEH, trainIHACRESuncal_bNSEH, trainIHACRESuncal_rH] = NSEH(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_NSEH, testIHACRESuncal_aNSEH, testIHACRESuncal_bNSEH, testIHACRESuncal_rH] = NSEH(testIHACRESuncal, testObs);
    [trainIHACRESuncal_NSEL, trainIHACRESuncal_aNSEL, trainIHACRESuncal_bNSEL, trainIHACRESuncal_rL] = NSEL(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_NSEL, testIHACRESuncal_aNSEL, testIHACRESuncal_bNSEL, testIHACRESuncal_rL] = NSEL(testIHACRESuncal, testObs);
    [trainIHACRESuncal_KGE, trainIHACRESuncal_aKGE, trainIHACRESuncal_bKGE] = KGE(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_KGE, testIHACRESuncal_aKGE, testIHACRESuncal_bKGE] = KGE(testIHACRESuncal, testObs);
    [trainIHACRESuncal_KGEL, trainIHACRESuncal_aKGEL, trainIHACRESuncal_bKGEL] = KGEL(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_KGEL, testIHACRESuncal_aKGEL, testIHACRESuncal_bKGEL] = KGEL(testIHACRESuncal, testObs);
    [trainIHACRESuncal_KGEH, trainIHACRESuncal_aKGEH, trainIHACRESuncal_bKGEH] = KGEH(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_KGEH, testIHACRESuncal_aKGEH, testIHACRESuncal_bKGEH] = KGEH(testIHACRESuncal, testObs);
    trainIHACRESuncal_FHV = FHV(trainIHACRESuncal, trainObs);
    testIHACRESuncal_FHV = FHV(testIHACRESuncal, testObs);
    trainIHACRESuncal_FLV = FLV(trainIHACRESuncal, trainObs);
    testIHACRESuncal_FLV = FLV(testIHACRESuncal, testObs);
    trainIHACRESuncal_FMS = FMS(trainIHACRESuncal, trainObs);
    testIHACRESuncal_FMS = FMS(testIHACRESuncal, testObs);
    [trainIHACRESuncal_FMR, trainIHACRESuncal_FMAR] = FMR(trainIHACRESuncal, trainObs);
    [testIHACRESuncal_FMR, testIHACRESuncal_FMAR] = FMR(testIHACRESuncal, testObs);
    trainIHACRESuncal_MI = MI(trainIHACRESuncal, trainObs);
    testIHACRESuncal_MI = MI(testIHACRESuncal, testObs);
    [trainIHACRESuncal_RR, trainIHACRESuncal_WRR, trainIHACRESuncal_DRR] = RR(trainIHACRESuncal, trainObs, trainRain, trainDates, area);
    [testIHACRESuncal_RR, testIHACRESuncal_WRR, testIHACRESuncal_DRR] = RR(testIHACRESuncal, testObs, testRain, testDates, area);
    trainIHACRESuncal_YVNSE = NSE(trainIHACRESuncalVol, trainObsVol);
    testIHACRESuncal_YVNSE = NSE(testIHACRESuncalVol, testObsVol);
    trainIHACRESuncal_YVRMSE = RMSE(trainIHACRESuncalVol, trainObsVol);
    testIHACRESuncal_YVRMSE = RMSE(testIHACRESuncalVol, testObsVol);
    trainIHACRESuncal_YVKGE = KGE(trainIHACRESuncalVol, trainObsVol);
    testIHACRESuncal_YVKGE = KGE(testIHACRESuncalVol, testObsVol);

    % added new metric
    [trainIHACRESuncal_SQRTNSE, trainIHACRESuncal_aQSRTNSE, trainIHACRESuncal_bSQRTNSE, trainIHACRESuncal_SQRTr] = NSE(sqrt(trainIHACRESuncal), sqrt(trainObs));
    [testIHACRESuncal_SQRTNSE, testIHACRESuncal_aSQRTNSE, testIHACRESuncal_bSQRTNSE, testIHACRESuncal_SQRTr] = NSE(sqrt(testIHACRESuncal), sqrt(testObs));

    % calibrated IHACRES
    [trainIHACREScalib_RMSE, trainIHACREScalib_NRMSE] = RMSE(trainIHACREScalib, trainObs);
    [testIHACREScalib_RMSE, testIHACREScalib_NRMSE] = RMSE(testIHACREScalib, testObs);
    trainIHACREScalib_R2 = RMSE(trainIHACREScalib, trainObs);
    testIHACREScalib_R2 = RMSE(testIHACREScalib, testObs);
    [trainIHACREScalib_NSE, trainIHACREScalib_aNSE, trainIHACREScalib_bNSE, trainIHACREScalib_r] = NSE(trainIHACREScalib, trainObs);
    [testIHACREScalib_NSE, testIHACREScalib_aNSE, testIHACREScalib_bNSE, testIHACREScalib_r] = NSE(testIHACREScalib, testObs);
    [trainIHACREScalib_NSEH, trainIHACREScalib_aNSEH, trainIHACREScalib_bNSEH, trainIHACREScalib_rH] = NSEH(trainIHACREScalib, trainObs);
    [testIHACREScalib_NSEH, testIHACREScalib_aNSEH, testIHACREScalib_bNSEH, testIHACREScalib_rH] = NSEH(testIHACREScalib, testObs);
    [trainIHACREScalib_NSEL, trainIHACREScalib_aNSEL, trainIHACREScalib_bNSEL, trainIHACREScalib_rL] = NSEL(trainIHACREScalib, trainObs);
    [testIHACREScalib_NSEL, testIHACREScalib_aNSEL, testIHACREScalib_bNSEL, testIHACREScalib_rL] = NSEL(testIHACREScalib, testObs);
    [trainIHACREScalib_KGE, trainIHACREScalib_aKGE, trainIHACREScalib_bKGE] = KGE(trainIHACREScalib, trainObs);
    [testIHACREScalib_KGE, testIHACREScalib_aKGE, testIHACREScalib_bKGE] = KGE(testIHACREScalib, testObs);
    [trainIHACREScalib_KGEL, trainIHACREScalib_aKGEL, trainIHACREScalib_bKGEL] = KGEL(trainIHACREScalib, trainObs);
    [testIHACREScalib_KGEL, testIHACREScalib_aKGEL, testIHACREScalib_bKGEL] = KGEL(testIHACREScalib, testObs);
    [trainIHACREScalib_KGEH, trainIHACREScalib_aKGEH, trainIHACREScalib_bKGEH] = KGEH(trainIHACREScalib, trainObs);
    [testIHACREScalib_KGEH, testIHACREScalib_aKGEH, testIHACREScalib_bKGEH] = KGEH(testIHACREScalib, testObs);
    trainIHACREScalib_FHV = FHV(trainIHACREScalib, trainObs);
    testIHACREScalib_FHV = FHV(testIHACREScalib, testObs);
    trainIHACREScalib_FLV = FLV(trainIHACREScalib, trainObs);
    testIHACREScalib_FLV = FLV(testIHACREScalib, testObs);
    trainIHACREScalib_FMS = FMS(trainIHACREScalib, trainObs);
    testIHACREScalib_FMS = FMS(testIHACREScalib, testObs);
    [trainIHACREScalib_FMR, trainIHACREScalib_FMAR] = FMR(trainIHACREScalib, trainObs);
    [testIHACREScalib_FMR, testIHACREScalib_FMAR] = FMR(testIHACREScalib, testObs);
    trainIHACREScalib_MI = MI(trainIHACREScalib, trainObs);
    testIHACREScalib_MI = MI(testIHACREScalib, testObs);
    [trainIHACREScalib_RR, trainIHACREScalib_WRR, trainIHACREScalib_DRR] = RR(trainIHACREScalib, trainObs, trainRain, trainDates, area);
    [testIHACREScalib_RR, testIHACREScalib_WRR, testIHACREScalib_DRR] = RR(testIHACREScalib, testObs, testRain, testDates, area);
    trainIHACREScalib_YVNSE = NSE(trainIHACREScalibVol, trainObsVol);
    testIHACREScalib_YVNSE = NSE(testIHACREScalibVol, testObsVol);
    trainIHACREScalib_YVRMSE = RMSE(trainIHACREScalibVol, trainObsVol);
    testIHACREScalib_YVRMSE = RMSE(testIHACREScalibVol, testObsVol);
    trainIHACREScalib_YVKGE = KGE(trainIHACREScalibVol, trainObsVol);
    testIHACREScalib_YVKGE = KGE(testIHACREScalibVol, testObsVol);

     % added new metric
    [trainIHACREScalib_SQRTNSE, trainIHACREScalib_aQSRTNSE, trainIHACREScalib_bSQRTNSE, trainIHACREScalib_SQRTr] = NSE(sqrt(trainIHACREScalib), sqrt(trainObs));
    [testIHACREScalib_SQRTNSE, testIHACREScalib_aSQRTNSE, testIHACREScalib_bSQRTNSE, testIHACREScalib_SQRTr] = NSE(sqrt(testIHACREScalib), sqrt(testObs));
   
    % allocate table
    %errTable = table('Size', [7,73], ...
    errTable = table('Size', [7,79], ...
        'VariableTypes', {...
            'string','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...           
            'double','double','double','double','double','double',... % new metrics
            }, ...
        'VariableNames', {...
            'Run_name','train_RMSE','test_RMSE','train_NRMSE','test_NRMSE','train_R2','test_R2', ...
            'train_NSE','test_NSE','train_aNSE','test_aNSE','train_bNSE','test_bNSE','train_r', 'test_r', ...
            'train_NSEL','test_NSEL','train_aNSEL','test_aNSEL','train_bNSEL','test_bNSEL','train_rL', 'test_rL', ...
            'train_NSEH','test_NSEH','train_aNSEH','test_aNSEH','train_bNSEH','test_bNSEH','train_rH', 'test_rH', ...
            'train_KGE','test_KGE','train_aKGE','test_aKGE','train_bKGE','test_bKGE', ...
            'train_FHV','test_FHV','train_FLV','test_FLV','train_FMS','test_FMS', ...
            'train_FMR','test_FMR','train_FMAR','test_FMAR','train_MI','test_MI', ...
            'train_RR', 'test_RR', 'train_WRR', 'test_WRR', 'train_DRR', 'test_DRR', ...
            'train_KGEL','test_KGEL','train_aKGEL','test_aKGEL','train_bKGEL','test_bKGEL', ...
            'train_KGEH','test_KGEH','train_aKGEH','test_aKGEH','train_bKGEH','test_bKGEH', ...
            'train_YVRMSE','test_YVRMSE','train_YVNSE','test_YVNSE','train_YVKGE','test_YVKGE', ...
            'train_SQRTNSE','test_SQRTNSE','train_MOTOTNSE','test_MOTOTNSE','train_LOSSVAR','test_LOSSVAR',... % new metrics
            });

    % fill table (LSTM)
    errTable(1,1) = VariableNames(3);
    %errTable(1,2:end) = array2table([ ...
    errTable(1,2:73) = array2table([ ...
        trainLSTM_RMSE, testLSTM_RMSE, trainLSTM_NRMSE, testLSTM_NRMSE, trainLSTM_R2, testLSTM_R2, ...
        trainLSTM_NSE, testLSTM_NSE, trainLSTM_aNSE, testLSTM_aNSE, trainLSTM_bNSE, testLSTM_bNSE, trainLSTM_r, testLSTM_r, ...
        trainLSTM_NSEL, testLSTM_NSEL, trainLSTM_aNSEL, testLSTM_aNSEL, trainLSTM_bNSEL, testLSTM_bNSEL, trainLSTM_rL, testLSTM_rL, ...
        trainLSTM_NSEH, testLSTM_NSEH, trainLSTM_aNSEH, testLSTM_aNSEH, trainLSTM_bNSEH, testLSTM_bNSEH, trainLSTM_rH, testLSTM_rH, ...
        trainLSTM_KGE, testLSTM_KGE, trainLSTM_aKGE, testLSTM_aKGE, trainLSTM_bKGE, testLSTM_bKGE, ...
        trainLSTM_FHV, testLSTM_FHV, trainLSTM_FLV, testLSTM_FLV, trainLSTM_FMS, testLSTM_FMS, ...
        trainLSTM_FMR, testLSTM_FMR, trainLSTM_FMAR, testLSTM_FMAR, trainLSTM_MI, testLSTM_MI, ...
        trainLSTM_RR, testLSTM_RR, trainLSTM_WRR, testLSTM_WRR, trainLSTM_DRR, testLSTM_DRR, ...
        trainLSTM_KGEL, testLSTM_KGEL, trainLSTM_aKGEL, testLSTM_aKGEL, trainLSTM_bKGEL, testLSTM_bKGEL, ...
        trainLSTM_KGEH, testLSTM_KGEH, trainLSTM_aKGEH, testLSTM_aKGEH, trainLSTM_bKGEH, testLSTM_bKGEH, ...
        trainLSTM_YVRMSE, testLSTM_YVRMSE, trainLSTM_YVNSE, testLSTM_YVNSE, trainLSTM_YVKGE, testLSTM_YVKGE, ...
        ]);
    % fill table (uncalibrated PRMS)
    errTable(2,1) = VariableNames(4);
    %errTable(2,2:end) = array2table([ ...
    errTable(2,2:73) = array2table([ ...
        trainPRMSuncal_RMSE, testPRMSuncal_RMSE, trainPRMSuncal_NRMSE, testPRMSuncal_NRMSE, trainPRMSuncal_R2, testPRMSuncal_R2, ...
        trainPRMSuncal_NSE, testPRMSuncal_NSE, trainPRMSuncal_aNSE, testPRMSuncal_aNSE, trainPRMSuncal_bNSE, testPRMSuncal_bNSE, trainPRMSuncal_r, testPRMSuncal_r, ...
        trainPRMSuncal_NSEL, testPRMSuncal_NSEL, trainPRMSuncal_aNSEL, testPRMSuncal_aNSEL, trainPRMSuncal_bNSEL, testPRMSuncal_bNSEL, trainPRMSuncal_rL, testPRMSuncal_rL, ...
        trainPRMSuncal_NSEH, testPRMSuncal_NSEH, trainPRMSuncal_aNSEH, testPRMSuncal_aNSEH, trainPRMSuncal_bNSEH, testPRMSuncal_bNSEH, trainPRMSuncal_rH, testPRMSuncal_rH, ...
        trainPRMSuncal_KGE, testPRMSuncal_KGE, trainPRMSuncal_aKGE, testPRMSuncal_aKGE, trainPRMSuncal_bKGE, testPRMSuncal_bKGE, ...
        trainPRMSuncal_FHV, testPRMSuncal_FHV, trainPRMSuncal_FLV, testPRMSuncal_FLV, trainPRMSuncal_FMS, testPRMSuncal_FMS, ...
        trainPRMSuncal_FMR, testPRMSuncal_FMR, trainPRMSuncal_FMAR, testPRMSuncal_FMAR, trainPRMSuncal_MI, testPRMSuncal_MI, ...
        trainPRMSuncal_RR, testPRMSuncal_RR, trainPRMSuncal_WRR, testPRMSuncal_WRR, trainPRMSuncal_DRR, testPRMSuncal_DRR, ...
        trainPRMSuncal_KGEL, testPRMSuncal_KGEL, trainPRMSuncal_aKGEL, testPRMSuncal_aKGEL, trainPRMSuncal_bKGEL, testPRMSuncal_bKGEL, ...
        trainPRMSuncal_KGEH, testPRMSuncal_KGEH, trainPRMSuncal_aKGEH, testPRMSuncal_aKGEH, trainPRMSuncal_bKGEH, testPRMSuncal_bKGEH, ...
        trainPRMSuncal_YVRMSE, testPRMSuncal_YVRMSE, trainPRMSuncal_YVNSE, testPRMSuncal_YVNSE, trainPRMSuncal_YVKGE, testPRMSuncal_YVKGE, ...
        ]);
    % fill table (calibrated PRMS)
    errTable(3,1) = VariableNames(5);
    %errTable(3,2:end) = array2table([ ...
    errTable(3,2:73) = array2table([ ...
        trainPRMScalib_RMSE, testPRMScalib_RMSE, trainPRMScalib_NRMSE, testPRMScalib_NRMSE, trainPRMScalib_R2, testPRMScalib_R2, ...
        trainPRMScalib_NSE, testPRMScalib_NSE, trainPRMScalib_aNSE, testPRMScalib_aNSE, trainPRMScalib_bNSE, testPRMScalib_bNSE, trainPRMScalib_r, testPRMScalib_r, ...
        trainPRMScalib_NSEL, testPRMScalib_NSEL, trainPRMScalib_aNSEL, testPRMScalib_aNSEL, trainPRMScalib_bNSEL, testPRMScalib_bNSEL, trainPRMScalib_rL, testPRMScalib_rL, ...
        trainPRMScalib_NSEH, testPRMScalib_NSEH, trainPRMScalib_aNSEH, testPRMScalib_aNSEH, trainPRMScalib_bNSEH, testPRMScalib_bNSEH, trainPRMScalib_rH, testPRMScalib_rH, ...
        trainPRMScalib_KGE, testPRMScalib_KGE, trainPRMScalib_aKGE, testPRMScalib_aKGE, trainPRMScalib_bKGE, testPRMScalib_bKGE, ...
        trainPRMScalib_FHV, testPRMScalib_FHV, trainPRMScalib_FLV, testPRMScalib_FLV, trainPRMScalib_FMS, testPRMScalib_FMS, ...
        trainPRMScalib_FMR, testPRMScalib_FMR, trainPRMScalib_FMAR, testPRMScalib_FMAR, trainPRMScalib_MI, testPRMScalib_MI, ...
        trainPRMScalib_RR, testPRMScalib_RR, trainPRMScalib_WRR, testPRMScalib_WRR, trainPRMScalib_DRR, testPRMScalib_DRR, ...
        trainPRMScalib_KGEL, testPRMScalib_KGEL, trainPRMScalib_aKGEL, testPRMScalib_aKGEL, trainPRMScalib_bKGEL, testPRMScalib_bKGEL, ...
        trainPRMScalib_KGEH, testPRMScalib_KGEH, trainPRMScalib_aKGEH, testPRMScalib_aKGEH, trainPRMScalib_bKGEH, testPRMScalib_bKGEH, ...
        trainPRMScalib_YVRMSE, testPRMScalib_YVRMSE, trainPRMScalib_YVNSE, testPRMScalib_YVNSE, trainPRMScalib_YVKGE, testPRMScalib_YVKGE, ...
        ]);
    % fill table (uncalibrated SACSMA)
    errTable(4,1) = VariableNames(6);
    %errTable(4,2:end) = array2table([ ...
    errTable(4,2:73) = array2table([ ...
        trainSACSMAuncal_RMSE, testSACSMAuncal_RMSE, trainSACSMAuncal_NRMSE, testSACSMAuncal_NRMSE, trainSACSMAuncal_R2, testSACSMAuncal_R2, ...
        trainSACSMAuncal_NSE, testSACSMAuncal_NSE, trainSACSMAuncal_aNSE, testSACSMAuncal_aNSE, trainSACSMAuncal_bNSE, testSACSMAuncal_bNSE, trainSACSMAuncal_r, testSACSMAuncal_r, ...
        trainSACSMAuncal_NSEL, testSACSMAuncal_NSEL, trainSACSMAuncal_aNSEL, testSACSMAuncal_aNSEL, trainSACSMAuncal_bNSEL, testSACSMAuncal_bNSEL, trainSACSMAuncal_rL, testSACSMAuncal_rL, ...
        trainSACSMAuncal_NSEH, testSACSMAuncal_NSEH, trainSACSMAuncal_aNSEH, testSACSMAuncal_aNSEH, trainSACSMAuncal_bNSEH, testSACSMAuncal_bNSEH, trainSACSMAuncal_rH, testSACSMAuncal_rH, ...
        trainSACSMAuncal_KGE, testSACSMAuncal_KGE, trainSACSMAuncal_aKGE, testSACSMAuncal_aKGE, trainSACSMAuncal_bKGE, testSACSMAuncal_bKGE, ...
        trainSACSMAuncal_FHV, testSACSMAuncal_FHV, trainSACSMAuncal_FLV, testSACSMAuncal_FLV, trainSACSMAuncal_FMS, testSACSMAuncal_FMS, ...
        trainSACSMAuncal_FMR, testSACSMAuncal_FMR, trainSACSMAuncal_FMAR, testSACSMAuncal_FMAR, trainSACSMAuncal_MI, testSACSMAuncal_MI, ...
        trainSACSMAuncal_RR, testSACSMAuncal_RR, trainSACSMAuncal_WRR, testSACSMAuncal_WRR, trainSACSMAuncal_DRR, testSACSMAuncal_DRR, ...
        trainSACSMAuncal_KGEL, testSACSMAuncal_KGEL, trainSACSMAuncal_aKGEL, testSACSMAuncal_aKGEL, trainSACSMAuncal_bKGEL, testSACSMAuncal_bKGEL, ...
        trainSACSMAuncal_KGEH, testSACSMAuncal_KGEH, trainSACSMAuncal_aKGEH, testSACSMAuncal_aKGEH, trainSACSMAuncal_bKGEH, testSACSMAuncal_bKGEH, ...
        trainSACSMAuncal_YVRMSE, testSACSMAuncal_YVRMSE, trainSACSMAuncal_YVNSE, testSACSMAuncal_YVNSE, trainSACSMAuncal_YVKGE, testSACSMAuncal_YVKGE, ...
        ]);
    % fill table (calibrated SACSMA)
    errTable(5,1) = VariableNames(7);
    %errTable(5,2:end) = array2table([ ...
    errTable(5,2:73) = array2table([ ...
        trainSACSMAcalib_RMSE, testSACSMAcalib_RMSE, trainSACSMAcalib_NRMSE, testSACSMAcalib_NRMSE, trainSACSMAcalib_R2, testSACSMAcalib_R2, ...
        trainSACSMAcalib_NSE, testSACSMAcalib_NSE, trainSACSMAcalib_aNSE, testSACSMAcalib_aNSE, trainSACSMAcalib_bNSE, testSACSMAcalib_bNSE, trainSACSMAcalib_r, testSACSMAcalib_r, ...
        trainSACSMAcalib_NSEL, testSACSMAcalib_NSEL, trainSACSMAcalib_aNSEL, testSACSMAcalib_aNSEL, trainSACSMAcalib_bNSEL, testSACSMAcalib_bNSEL, trainSACSMAcalib_rL, testSACSMAcalib_rL, ...
        trainSACSMAcalib_NSEH, testSACSMAcalib_NSEH, trainSACSMAcalib_aNSEH, testSACSMAcalib_aNSEH, trainSACSMAcalib_bNSEH, testSACSMAcalib_bNSEH, trainSACSMAcalib_rH, testSACSMAcalib_rH, ...
        trainSACSMAcalib_KGE, testSACSMAcalib_KGE, trainSACSMAcalib_aKGE, testSACSMAcalib_aKGE, trainSACSMAcalib_bKGE, testSACSMAcalib_bKGE, ...
        trainSACSMAcalib_FHV, testSACSMAcalib_FHV, trainSACSMAcalib_FLV, testSACSMAcalib_FLV, trainSACSMAcalib_FMS, testSACSMAcalib_FMS, ...
        trainSACSMAcalib_FMR, testSACSMAcalib_FMR, trainSACSMAcalib_FMAR, testSACSMAcalib_FMAR, trainSACSMAcalib_MI, testSACSMAcalib_MI, ...
        trainSACSMAcalib_RR, testSACSMAcalib_RR, trainSACSMAcalib_WRR, testSACSMAcalib_WRR, trainSACSMAcalib_DRR, testSACSMAcalib_DRR, ...
        trainSACSMAcalib_KGEL, testSACSMAcalib_KGEL, trainSACSMAcalib_aKGEL, testSACSMAcalib_aKGEL, trainSACSMAcalib_bKGEL, testSACSMAcalib_bKGEL, ...
        trainSACSMAcalib_KGEH, testSACSMAcalib_KGEH, trainSACSMAcalib_aKGEH, testSACSMAcalib_aKGEH, trainSACSMAcalib_bKGEH, testSACSMAcalib_bKGEH, ...
        trainSACSMAcalib_YVRMSE, testSACSMAcalib_YVRMSE, trainSACSMAcalib_YVNSE, testSACSMAcalib_YVNSE, trainSACSMAcalib_YVKGE, testSACSMAcalib_YVKGE, ...
       ]);
    
    % fill table (uncalibrated IHACRES)
    errTable(6,1) = VariableNames(8);
    %errTable(6,2:end) = array2table([ ...
    errTable(6,2:73) = array2table([ ...
        trainIHACRESuncal_RMSE, testIHACRESuncal_RMSE, trainIHACRESuncal_NRMSE, testIHACRESuncal_NRMSE, trainIHACRESuncal_R2, testIHACRESuncal_R2, ...
        trainIHACRESuncal_NSE, testIHACRESuncal_NSE, trainIHACRESuncal_aNSE, testIHACRESuncal_aNSE, trainIHACRESuncal_bNSE, testIHACRESuncal_bNSE, trainIHACRESuncal_r, testIHACRESuncal_r, ...
        trainIHACRESuncal_NSEL, testIHACRESuncal_NSEL, trainIHACRESuncal_aNSEL, testIHACRESuncal_aNSEL, trainIHACRESuncal_bNSEL, testIHACRESuncal_bNSEL, trainIHACRESuncal_rL, testIHACRESuncal_rL, ...
        trainIHACRESuncal_NSEH, testIHACRESuncal_NSEH, trainIHACRESuncal_aNSEH, testIHACRESuncal_aNSEH, trainIHACRESuncal_bNSEH, testIHACRESuncal_bNSEH, trainIHACRESuncal_rH, testIHACRESuncal_rH, ...
        trainIHACRESuncal_KGE, testIHACRESuncal_KGE, trainIHACRESuncal_aKGE, testIHACRESuncal_aKGE, trainIHACRESuncal_bKGE, testIHACRESuncal_bKGE, ...
        trainIHACRESuncal_FHV, testIHACRESuncal_FHV, trainIHACRESuncal_FLV, testIHACRESuncal_FLV, trainIHACRESuncal_FMS, testIHACRESuncal_FMS, ...
        trainIHACRESuncal_FMR, testIHACRESuncal_FMR, trainIHACRESuncal_FMAR, testIHACRESuncal_FMAR, trainIHACRESuncal_MI, testIHACRESuncal_MI, ...
        trainIHACRESuncal_RR, testIHACRESuncal_RR, trainIHACRESuncal_WRR, testIHACRESuncal_WRR, trainIHACRESuncal_DRR, testIHACRESuncal_DRR, ...
        trainIHACRESuncal_KGEL, testIHACRESuncal_KGEL, trainIHACRESuncal_aKGEL, testIHACRESuncal_aKGEL, trainIHACRESuncal_bKGEL, testIHACRESuncal_bKGEL, ...
        trainIHACRESuncal_KGEH, testIHACRESuncal_KGEH, trainIHACRESuncal_aKGEH, testIHACRESuncal_aKGEH, trainIHACRESuncal_bKGEH, testIHACRESuncal_bKGEH, ...
        trainIHACRESuncal_YVRMSE, testIHACRESuncal_YVRMSE, trainIHACRESuncal_YVNSE, testIHACRESuncal_YVNSE, trainIHACRESuncal_YVKGE, testIHACRESuncal_YVKGE, ...
        ]);
    % fill table (calibrated IHACRES)
    errTable(7,1) = VariableNames(9);
    %errTable(7,2:end) = array2table([ ...
    errTable(7,2:73) = array2table([ ...
        trainIHACREScalib_RMSE, testIHACREScalib_RMSE, trainIHACREScalib_NRMSE, testIHACREScalib_NRMSE, trainIHACREScalib_R2, testIHACREScalib_R2, ...
        trainIHACREScalib_NSE, testIHACREScalib_NSE, trainIHACREScalib_aNSE, testIHACREScalib_aNSE, trainIHACREScalib_bNSE, testIHACREScalib_bNSE, trainIHACREScalib_r, testIHACREScalib_r, ...
        trainIHACREScalib_NSEL, testIHACREScalib_NSEL, trainIHACREScalib_aNSEL, testIHACREScalib_aNSEL, trainIHACREScalib_bNSEL, testIHACREScalib_bNSEL, trainIHACREScalib_rL, testIHACREScalib_rL, ...
        trainIHACREScalib_NSEH, testIHACREScalib_NSEH, trainIHACREScalib_aNSEH, testIHACREScalib_aNSEH, trainIHACREScalib_bNSEH, testIHACREScalib_bNSEH, trainIHACREScalib_rH, testIHACREScalib_rH, ...
        trainIHACREScalib_KGE, testIHACREScalib_KGE, trainIHACREScalib_aKGE, testIHACREScalib_aKGE, trainIHACREScalib_bKGE, testIHACREScalib_bKGE, ...
        trainIHACREScalib_FHV, testIHACREScalib_FHV, trainIHACREScalib_FLV, testIHACREScalib_FLV, trainIHACREScalib_FMS, testIHACREScalib_FMS, ...
        trainIHACREScalib_FMR, testIHACREScalib_FMR, trainIHACREScalib_FMAR, testIHACREScalib_FMAR, trainIHACREScalib_MI, testIHACREScalib_MI, ...
        trainIHACREScalib_RR, testIHACREScalib_RR, trainIHACREScalib_WRR, testIHACREScalib_WRR, trainIHACREScalib_DRR, testIHACREScalib_DRR, ...
        trainIHACREScalib_KGEL, testIHACREScalib_KGEL, trainIHACREScalib_aKGEL, testIHACREScalib_aKGEL, trainIHACREScalib_bKGEL, testIHACREScalib_bKGEL, ...
        trainIHACREScalib_KGEH, testIHACREScalib_KGEH, trainIHACREScalib_aKGEH, testIHACREScalib_aKGEH, trainIHACREScalib_bKGEH, testIHACREScalib_bKGEH, ...
        trainIHACREScalib_YVRMSE, testIHACREScalib_YVRMSE, trainIHACREScalib_YVNSE, testIHACREScalib_YVNSE, trainIHACREScalib_YVKGE, testIHACREScalib_YVKGE, ...
        ]);
    
    %% yearly error metrics
    % allocate table
    yearlyTable = table('Size', [numWY,1+7*10], ...
        'VariableTypes', {...
            'double',...
            'double','double','double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double','double','double',...
            }, ...
        'VariableNames', {...
            'Water_Year',...
            ['NSE_' runName],['KGE_' runName],['RR_' runName],['FHV_' runName],['FLV_' runName],['FMS_' runName],['FDCB_' runName],['FDCB_LT100_' runName],['FDCB_GT100_' runName],['MDFB_' runName], ...
            'NSE_PRMSuncal','KGE_PRMSuncal','RR_PRMSuncal','FHV_PRMSuncal','FLV_PRMSuncal','FMS_PRMSuncal','FDCB_PRMSuncal','FDCB_LT100_PRMSuncal','FDCB_GT100_PRMSuncal','MDFB_PRMSuncal', ...
            'NSE_PRMScalib','KGE_PRMScalib','RR_PRMScalib','FHV_PRMScalib','FLV_PRMScalib','FMS_PRMScalib','FDCB_PRMScalib','FDCB_LT100_PRMScalib','FDCB_GT100_PRMScalib','MDFB_PRMScalib', ...
            'NSE_SACSMAuncal','KGE_SACSMAuncal','RR_SACSMAuncal','FHV_SACSMAuncal','FLV_SACSMAuncal','FMS_SACSMAuncal','FDCB_SACSMAuncal','FDCB_LT100_SACSMAuncal','FDCB_GT100_SACSMAuncal','MDFB_SACSMAuncal', ...
            'NSE_SACSMAcalib','KGE_SACSMAcalib','RR_SACSMAcalib','FHV_SACSMAcalib','FLV_SACSMAcalib','FMS_SACSMAcalib','FDCB_SACSMAcalib','FDCB_LT100_SACSMAcalib','FDCB_GT100_SACSMAcalib','MDFB_SACSMAcalib', ...
            'NSE_IHACRESuncal','KGE_IHACRESuncal','RR_IHACRESuncal','FHV_IHACRESuncal','FLV_IHACRESuncal','FMS_IHACRESuncal','FDCB_IHACRESuncal','FDCB_LT100_IHACRESuncal','FDCB_GT100_IHACRESuncal','MDFB_IHACRESuncal', ...
            'NSE_IHACREScalib','KGE_IHACREScalib','RR_IHACREScalib','FHV_IHACREScalib','FLV_IHACREScalib','FMS_IHACREScalib','FDCB_IHACREScalib','FDCB_LT100_IHACREScalib','FDCB_GT100_IHACREScalib','MDFB_IHACREScalib', ...
            });
    
    % loop over water years
    n = 0;
    for myYear = begWY:endWY
        % get indices and variables
        n = n+1;
        indWY = (allWYears == myYear);
        myObs = allObs(indWY);
        myLSTM = allLSTM(indWY);
        myPRMSuncal = allPRMSuncal(indWY);
        myPRMScalib = allPRMScalib(indWY);
        mySACSMAuncal = allSACSMAuncal(indWY);
        mySACSMAcalib = allSACSMAcalib(indWY);
        myIHACRESuncal = allIHACRESuncal(indWY);
        myIHACREScalib = allIHACREScalib(indWY);
        myRain = allRain(indWY);
        myDates = allDates(indWY);
        
        % compute LSTM metrics
        myNSE_LSTM = NSE(myLSTM, myObs);
        myKGE_LSTM = KGE(myLSTM, myObs);
        myRR_LSTM = RR(myLSTM, myObs, myRain, myDates, area);
        myFHV_LSTM = FHV(myLSTM, myObs);
        myFLV_LSTM = FLV(myLSTM, myObs);
        myFMS_LSTM = FMS(myLSTM, myObs);
        [myFDCB_LSTM, myFDCB_LT100_LSTM, myFDCB_GT100_LSTM] = FDCB(myLSTM, myObs);
        myMDFB_LSTM = MDFB(myLSTM, myObs);
        
        % compute PRMSuncal metrics
        myNSE_PRMSuncal = NSE(myPRMSuncal, myObs);
        myKGE_PRMSuncal = KGE(myPRMSuncal, myObs);
        myRR_PRMSuncal = RR(myPRMSuncal, myObs, myRain, myDates, area);
        myFHV_PRMSuncal = FHV(myPRMSuncal, myObs);
        myFLV_PRMSuncal = FLV(myPRMSuncal, myObs);
        myFMS_PRMSuncal = FMS(myPRMSuncal, myObs);
        [myFDCB_PRMSuncal, myFDCB_LT100_PRMSuncal, myFDCB_GT100_PRMSuncal] = FDCB(myPRMSuncal, myObs);
        myMDFB_PRMSuncal = MDFB(myPRMSuncal, myObs);
        
        % compute PRMScalib metrics
        myNSE_PRMScalib = NSE(myPRMScalib, myObs);
        myKGE_PRMScalib = KGE(myPRMScalib, myObs);
        myRR_PRMScalib = RR(myPRMScalib, myObs, myRain, myDates, area);
        myFHV_PRMScalib = FHV(myPRMScalib, myObs);
        myFLV_PRMScalib = FLV(myPRMScalib, myObs);
        myFMS_PRMScalib = FMS(myPRMScalib, myObs);
        [myFDCB_PRMScalib, myFDCB_LT100_PRMScalib, myFDCB_GT100_PRMScalib] = FDCB(myPRMScalib, myObs);
        myMDFB_PRMScalib = MDFB(myPRMScalib, myObs);
        
        % compute SACSMAuncal metrics
        myNSE_SACSMAuncal = NSE(mySACSMAuncal, myObs);
        myKGE_SACSMAuncal = KGE(mySACSMAuncal, myObs);
        myRR_SACSMAuncal = RR(mySACSMAuncal, myObs, myRain, myDates, area);
        myFHV_SACSMAuncal = FHV(mySACSMAuncal, myObs);
        myFLV_SACSMAuncal = FLV(mySACSMAuncal, myObs);
        myFMS_SACSMAuncal = FMS(mySACSMAuncal, myObs);
        [myFDCB_SACSMAuncal, myFDCB_LT100_SACSMAuncal, myFDCB_GT100_SACSMAuncal] = FDCB(mySACSMAuncal, myObs);
        myMDFB_SACSMAuncal = MDFB(mySACSMAuncal, myObs);
        
        % compute SACSMAcalib metrics
        myNSE_SACSMAcalib = NSE(mySACSMAcalib, myObs);
        myKGE_SACSMAcalib = KGE(mySACSMAcalib, myObs);
        myRR_SACSMAcalib = RR(mySACSMAcalib, myObs, myRain, myDates, area);
        myFHV_SACSMAcalib = FHV(mySACSMAcalib, myObs);
        myFLV_SACSMAcalib = FLV(mySACSMAcalib, myObs);
        myFMS_SACSMAcalib = FMS(mySACSMAcalib, myObs);
        [myFDCB_SACSMAcalib, myFDCB_LT100_SACSMAcalib, myFDCB_GT100_SACSMAcalib] = FDCB(mySACSMAcalib, myObs);
        myMDFB_SACSMAcalib = MDFB(mySACSMAcalib, myObs);
        
        % compute IHACRESuncal metrics
        myNSE_IHACRESuncal = NSE(myIHACRESuncal, myObs);
        myKGE_IHACRESuncal = KGE(myIHACRESuncal, myObs);
        myRR_IHACRESuncal = RR(myIHACRESuncal, myObs, myRain, myDates, area);
        myFHV_IHACRESuncal = FHV(myIHACRESuncal, myObs);
        myFLV_IHACRESuncal = FLV(myIHACRESuncal, myObs);
        myFMS_IHACRESuncal = FMS(myIHACRESuncal, myObs);
        [myFDCB_IHACRESuncal, myFDCB_LT100_IHACRESuncal, myFDCB_GT100_IHACRESuncal] = FDCB(myIHACRESuncal, myObs);
        myMDFB_IHACRESuncal = MDFB(myIHACRESuncal, myObs);
        
        % compute IHACREScalib metrics
        myNSE_IHACREScalib = NSE(myIHACREScalib, myObs);
        myKGE_IHACREScalib = KGE(myIHACREScalib, myObs);
        myRR_IHACREScalib = RR(myIHACREScalib, myObs, myRain, myDates, area);
        myFHV_IHACREScalib = FHV(myIHACREScalib, myObs);
        myFLV_IHACREScalib = FLV(myIHACREScalib, myObs);
        myFMS_IHACREScalib = FMS(myIHACREScalib, myObs);
        [myFDCB_IHACREScalib, myFDCB_LT100_IHACREScalib, myFDCB_GT100_IHACREScalib] = FDCB(myIHACREScalib, myObs);
        myMDFB_IHACREScalib = MDFB(myIHACREScalib, myObs);
        
        % fill table row
        yearlyTable(n,1) = array2table(myYear);
        yearlyTable(n,2:11) = array2table([myNSE_LSTM,myKGE_LSTM,myRR_LSTM,myFHV_LSTM,myFLV_LSTM,myFMS_LSTM,myFDCB_LSTM,myFDCB_LT100_LSTM,myFDCB_GT100_LSTM,myMDFB_LSTM]);
        yearlyTable(n,12:21) = array2table([myNSE_PRMSuncal,myKGE_PRMSuncal,myRR_PRMSuncal,myFHV_PRMSuncal,myFLV_PRMSuncal,myFMS_PRMSuncal,myFDCB_PRMSuncal,myFDCB_LT100_PRMSuncal,myFDCB_GT100_PRMSuncal,myMDFB_PRMSuncal]);
        yearlyTable(n,22:31) = array2table([myNSE_PRMScalib,myKGE_PRMScalib,myRR_PRMScalib,myFHV_PRMScalib,myFLV_PRMScalib,myFMS_PRMScalib,myFDCB_PRMScalib,myFDCB_LT100_PRMScalib,myFDCB_GT100_PRMScalib,myMDFB_PRMScalib]);
        yearlyTable(n,32:41) = array2table([myNSE_SACSMAuncal,myKGE_SACSMAuncal,myRR_SACSMAuncal,myFHV_SACSMAuncal,myFLV_SACSMAuncal,myFMS_SACSMAuncal,myFDCB_SACSMAuncal,myFDCB_LT100_SACSMAuncal,myFDCB_GT100_SACSMAuncal,myMDFB_SACSMAuncal]);
        yearlyTable(n,42:51) = array2table([myNSE_SACSMAcalib,myKGE_SACSMAcalib,myRR_SACSMAcalib,myFHV_SACSMAcalib,myFLV_SACSMAcalib,myFMS_SACSMAcalib,myFDCB_SACSMAcalib,myFDCB_LT100_SACSMAcalib,myFDCB_GT100_SACSMAcalib,myMDFB_SACSMAcalib]);
        yearlyTable(n,52:61) = array2table([myNSE_IHACRESuncal,myKGE_IHACRESuncal,myRR_IHACRESuncal,myFHV_IHACRESuncal,myFLV_IHACRESuncal,myFMS_IHACRESuncal,myFDCB_IHACRESuncal,myFDCB_LT100_IHACRESuncal,myFDCB_GT100_IHACRESuncal,myMDFB_IHACRESuncal]);
        yearlyTable(n,62:71) = array2table([myNSE_IHACREScalib,myKGE_IHACREScalib,myRR_IHACREScalib,myFHV_IHACREScalib,myFLV_IHACREScalib,myFMS_IHACREScalib,myFDCB_IHACREScalib,myFDCB_LT100_IHACREScalib,myFDCB_GT100_IHACREScalib,myMDFB_IHACREScalib]);
    end

    %% monthly error metrics
    numMO = 12;
    Months = [10,11,12,1,2,3,4,5,6,7,8,9];

    % allocate table
    %monthlyTable = table('Size', [numWY*numMO,2+6*7], ...
    monthlyTable = table('Size', [numWY*numMO,2+6*7+9], ... % added totals (45-53)
        'VariableTypes', {...
            'double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double',...
            'double','double','double','double','double','double','double','double','double',... %added totals
            }, ...
        'VariableNames', {...
            'Water_Year','Month',...
            ['RR_' runName],['FDCB_' runName],['FDCB_LT100_' runName],['FDCB_GT100_' runName],['MDFB_' runName],['MFB_' runName], ...
            'RR_PRMSuncal','FDCB_PRMSuncal','FDCB_LT100_PRMSuncal','FDCB_GT100_PRMSuncal','MDFB_PRMSuncal','MFB_PRMSuncal', ...
            'RR_PRMScalib','FDCB_PRMScalib','FDCB_LT100_PRMScalib','FDCB_GT100_PRMScalib','MDFB_PRMScalib','MFB_PRMScalib', ...
            'RR_SACSMAuncal','FDCB_SACSMAuncal','FDCB_LT100_SACSMAuncal','FDCB_GT100_SACSMAuncal','MDFB_SACSMAuncal','MFB_SACSMAuncal', ...
            'RR_SACSMAcalib','FDCB_SACSMAcalib','FDCB_LT100_SACSMAcalib','FDCB_GT100_SACSMAcalib','MDFB_SACSMAcalib','MFB_SACSMAcalib', ...
            'RR_IHACRESuncal','FDCB_IHACRESuncal','FDCB_LT100_IHACRESuncal','FDCB_GT100_IHACRESuncal','MDFB_IHACRESuncal','MFB_IHACRESuncal', ...
            'RR_IHACREScalib','FDCB_IHACREScalib','FDCB_LT100_IHACREScalib','FDCB_GT100_IHACREScalib','MDFB_IHACREScalib','MFB_IHACREScalib', ...
            'moObsTot','moLSTMTot','moPRMSuncalTot','moPRMScalibTot','moSACSMAuncalTot','moSACSMAcalibTot','moIHACRESuncalTot','moIHACREScalibTot','moRainTot', ... % added totals
            });
    
    % loop over water years
    n = 0;
    for myYear = begWY:endWY
        % get indices and variables
        indWY = (allWYears == myYear);
        yrObs = allObs(indWY);
        yrLSTM = allLSTM(indWY);
        yrPRMSuncal = allPRMSuncal(indWY);
        yrPRMScalib = allPRMScalib(indWY);
        yrSACSMAuncal = allSACSMAuncal(indWY);
        yrSACSMAcalib = allSACSMAcalib(indWY);
        yrIHACRESuncal = allIHACRESuncal(indWY);
        yrIHACREScalib = allIHACREScalib(indWY);
        yrRain = allRain(indWY);
        yrDates = allDates(indWY);
        
        % loop over months
        for myMonth = Months
            n = n+1;

            % get indices and variables
            indMO = (month(yrDates) == myMonth);
            moObs = yrObs(indMO);
            moLSTM = yrLSTM(indMO);
            moPRMSuncal = yrPRMSuncal(indMO);
            moPRMScalib = yrPRMScalib(indMO);
            moSACSMAuncal = yrSACSMAuncal(indMO);
            moSACSMAcalib = yrSACSMAcalib(indMO);
            moIHACRESuncal = yrIHACRESuncal(indMO);
            moIHACREScalib = yrIHACREScalib(indMO);
            moRain = yrRain(indMO);
            moDates = yrDates(indMO);

            % get monthly totals
            moObsTot = sum(moObs);
            moLSTMTot = sum(moLSTM);
            moPRMSuncalTot = sum(moPRMSuncal);
            moPRMScalibTot = sum(moPRMScalib);
            moSACSMAuncalTot = sum(moSACSMAuncal);
            moSACSMAcalibTot = sum(moSACSMAcalib);
            moIHACRESuncalTot = sum(moIHACRESuncal);
            moIHACREScalibTot = sum(moIHACREScalib);
            moRainTot = sum(moRain);            
            
            % compute LSTM metrics
            moRR_LSTM = RR(moLSTM, moObs, moRain, moDates, area);
            [moFDCB_LSTM, moFDCB_LT100_LSTM, moFDCB_GT100_LSTM] = FDCB(moLSTM, moObs);
            moMDFB_LSTM = MDFB(moLSTM, moObs);
            moMFB_LSTM = MFB(moLSTM, moObs);
            
            % compute PRMSuncal metrics
            moRR_PRMSuncal = RR(moPRMSuncal, moObs, moRain, moDates, area);
            [moFDCB_PRMSuncal, moFDCB_LT100_PRMSuncal, moFDCB_GT100_PRMSuncal] = FDCB(moPRMSuncal, moObs);
            moMDFB_PRMSuncal = MDFB(moPRMSuncal, moObs);
            moMFB_PRMSuncal = MFB(moPRMSuncal, moObs);
            
            % compute PRMScalib metrics
            moRR_PRMScalib = RR(moPRMScalib, moObs, moRain, moDates, area);
            [moFDCB_PRMScalib, moFDCB_LT100_PRMScalib, moFDCB_GT100_PRMScalib] = FDCB(moPRMScalib, moObs);
            moMDFB_PRMScalib = MDFB(moPRMScalib, moObs);
            moMFB_PRMScalib = MFB(moPRMScalib, moObs);
            
            % compute SACSMAuncal metrics
            moRR_SACSMAuncal = RR(moSACSMAuncal, moObs, moRain, moDates, area);
            [moFDCB_SACSMAuncal, moFDCB_LT100_SACSMAuncal, moFDCB_GT100_SACSMAuncal] = FDCB(moSACSMAuncal, moObs);
            moMDFB_SACSMAuncal = MDFB(moSACSMAuncal, moObs);
            moMFB_SACSMAuncal = MFB(moSACSMAuncal, moObs);
            
            % compute SACSMAcalib metrics
            moRR_SACSMAcalib = RR(moSACSMAcalib, moObs, moRain, moDates, area);
            [moFDCB_SACSMAcalib, moFDCB_LT100_SACSMAcalib, moFDCB_GT100_SACSMAcalib] = FDCB(moSACSMAcalib, moObs);
            moMDFB_SACSMAcalib = MDFB(moSACSMAcalib, moObs);
            moMFB_SACSMAcalib = MFB(moSACSMAcalib, moObs);
            
            % compute IHACRESuncal metrics
            moRR_IHACRESuncal = RR(moIHACRESuncal, moObs, moRain, moDates, area);
            [moFDCB_IHACRESuncal, moFDCB_LT100_IHACRESuncal, moFDCB_GT100_IHACRESuncal] = FDCB(moIHACRESuncal, moObs);
            moMDFB_IHACRESuncal = MDFB(moIHACRESuncal, moObs);
            moMFB_IHACRESuncal = MFB(moIHACRESuncal, moObs);
            
            % compute IHACREScalib metrics
            moRR_IHACREScalib = RR(moIHACREScalib, moObs, moRain, moDates, area);
            [moFDCB_IHACREScalib, moFDCB_LT100_IHACREScalib, moFDCB_GT100_IHACREScalib] = FDCB(moIHACREScalib, moObs);
            moMDFB_IHACREScalib = MDFB(moIHACREScalib, moObs);
            moMFB_IHACREScalib = MFB(moIHACREScalib, moObs);
            
            % fill table row
            monthlyTable(n,1:2) = array2table([myYear,myMonth]);
            monthlyTable(n,3:8) = array2table([moRR_LSTM,moFDCB_LSTM,moFDCB_LT100_LSTM,moFDCB_GT100_LSTM,moMDFB_LSTM,moMFB_LSTM]);
            monthlyTable(n,9:14) = array2table([moRR_PRMSuncal,moFDCB_PRMSuncal,moFDCB_LT100_PRMSuncal,moFDCB_GT100_PRMSuncal,moMDFB_PRMSuncal,moMFB_PRMSuncal]);
            monthlyTable(n,15:20) = array2table([moRR_PRMScalib,moFDCB_PRMScalib,moFDCB_LT100_PRMScalib,moFDCB_GT100_PRMScalib,moMDFB_PRMScalib,moMFB_PRMScalib]);
            monthlyTable(n,21:26) = array2table([moRR_SACSMAuncal,moFDCB_SACSMAuncal,moFDCB_LT100_SACSMAuncal,moFDCB_GT100_SACSMAuncal,moMDFB_SACSMAuncal,moMFB_SACSMAuncal]);
            monthlyTable(n,27:32) = array2table([moRR_SACSMAcalib,moFDCB_SACSMAcalib,moFDCB_LT100_SACSMAcalib,moFDCB_GT100_SACSMAcalib,moMDFB_SACSMAcalib,moMFB_SACSMAcalib]);
            monthlyTable(n,33:38) = array2table([moRR_IHACRESuncal,moFDCB_IHACRESuncal,moFDCB_LT100_IHACRESuncal,moFDCB_GT100_IHACRESuncal,moMDFB_IHACRESuncal,moMFB_IHACRESuncal]);
            monthlyTable(n,39:44) = array2table([moRR_IHACREScalib,moFDCB_IHACREScalib,moFDCB_LT100_IHACREScalib,moFDCB_GT100_IHACREScalib,moMDFB_IHACREScalib,moMFB_IHACREScalib]);
            monthlyTable(n,45:53) = array2table([moObsTot,moLSTMTot,moPRMSuncalTot,moPRMScalibTot,moSACSMAuncalTot,moSACSMAcalibTot,moIHACRESuncalTot,moIHACREScalibTot,moRainTot]); % added totals
        end
    end
    
    %% add new metrics (square root NSE monthly total NSE, weighted average) to error table
    
    % divide months in train and test
    trainMonths = false(numWY*numMO,1);
    trainMonths(ismember(table2array(monthlyTable(:,1)),trainWYears)) = true;
    testMonths = ~trainMonths;

    % compute monthly NSEs
    trainLSTM_MOTOTNSE = NSE(table2array(monthlyTable(trainMonths,46)),table2array(monthlyTable(trainMonths,45)));
    trainPRMSuncal_MOTOTNSE = NSE(table2array(monthlyTable(trainMonths,47)),table2array(monthlyTable(trainMonths,45)));
    trainPRMScalib_MOTOTNSE = NSE(table2array(monthlyTable(trainMonths,48)),table2array(monthlyTable(trainMonths,45)));
    trainSACSMAuncal_MOTOTNSE = NSE(table2array(monthlyTable(trainMonths,49)),table2array(monthlyTable(trainMonths,45)));
    trainSACSMAcalib_MOTOTNSE = NSE(table2array(monthlyTable(trainMonths,50)),table2array(monthlyTable(trainMonths,45)));
    trainIHACRESuncal_MOTOTNSE = NSE(table2array(monthlyTable(trainMonths,51)),table2array(monthlyTable(trainMonths,45)));
    trainIHACREScalib_MOTOTNSE = NSE(table2array(monthlyTable(trainMonths,52)),table2array(monthlyTable(trainMonths,45)));
    testLSTM_MOTOTNSE = NSE(table2array(monthlyTable(testMonths,46)),table2array(monthlyTable(testMonths,45)));
    testPRMSuncal_MOTOTNSE = NSE(table2array(monthlyTable(testMonths,47)),table2array(monthlyTable(testMonths,45)));
    testPRMScalib_MOTOTNSE = NSE(table2array(monthlyTable(testMonths,48)),table2array(monthlyTable(testMonths,45)));
    testSACSMAuncal_MOTOTNSE = NSE(table2array(monthlyTable(testMonths,49)),table2array(monthlyTable(testMonths,45)));
    testSACSMAcalib_MOTOTNSE = NSE(table2array(monthlyTable(testMonths,50)),table2array(monthlyTable(testMonths,45)));
    testIHACRESuncal_MOTOTNSE = NSE(table2array(monthlyTable(testMonths,51)),table2array(monthlyTable(testMonths,45)));
    testIHACREScalib_MOTOTNSE = NSE(table2array(monthlyTable(testMonths,52)),table2array(monthlyTable(testMonths,45)));
    
    % compute weighted average
    trainLSTM_LOSSVAR = 0.7 * trainLSTM_SQRTNSE + 0.3 * trainLSTM_MOTOTNSE;
    trainPRMSuncal_LOSSVAR = 0.7 * trainPRMSuncal_SQRTNSE + 0.3 * trainPRMSuncal_MOTOTNSE;
    trainPRMScalib_LOSSVAR = 0.7 * trainPRMScalib_SQRTNSE + 0.3 * trainPRMScalib_MOTOTNSE;
    trainSACSMAuncal_LOSSVAR = 0.7 * trainSACSMAuncal_SQRTNSE + 0.3 * trainSACSMAuncal_MOTOTNSE;
    trainSACSMAcalib_LOSSVAR = 0.7 * trainSACSMAcalib_SQRTNSE + 0.3 * trainSACSMAcalib_MOTOTNSE;
    trainIHACRESuncal_LOSSVAR = 0.7 * trainIHACRESuncal_SQRTNSE + 0.3 * trainIHACRESuncal_MOTOTNSE;
    trainIHACREScalib_LOSSVAR = 0.7 * trainIHACREScalib_SQRTNSE + 0.3 * trainIHACREScalib_MOTOTNSE;
    testLSTM_LOSSVAR = 0.7 * testLSTM_SQRTNSE + 0.3 * testLSTM_MOTOTNSE;
    testPRMSuncal_LOSSVAR = 0.7 * testPRMSuncal_SQRTNSE + 0.3 * testPRMSuncal_MOTOTNSE;
    testPRMScalib_LOSSVAR = 0.7 * testPRMScalib_SQRTNSE + 0.3 * testPRMScalib_MOTOTNSE;
    testSACSMAuncal_LOSSVAR = 0.7 * testSACSMAuncal_SQRTNSE + 0.3 * testSACSMAuncal_MOTOTNSE;
    testSACSMAcalib_LOSSVAR = 0.7 * testSACSMAcalib_SQRTNSE + 0.3 * testSACSMAcalib_MOTOTNSE;
    testIHACRESuncal_LOSSVAR = 0.7 * testIHACRESuncal_SQRTNSE + 0.3 * testIHACRESuncal_MOTOTNSE;
    testIHACREScalib_LOSSVAR = 0.7 * testIHACREScalib_SQRTNSE + 0.3 * testIHACREScalib_MOTOTNSE;
    
    % fill table (LSTM,SACSMA,IHACRES)
    errTable(1,74:end) = array2table([trainLSTM_SQRTNSE,testLSTM_SQRTNSE,trainLSTM_MOTOTNSE,testLSTM_MOTOTNSE,trainLSTM_LOSSVAR,testLSTM_LOSSVAR]);
    errTable(2,74:end) = array2table([trainPRMSuncal_SQRTNSE,testPRMSuncal_SQRTNSE,trainPRMSuncal_MOTOTNSE,testPRMSuncal_MOTOTNSE,trainPRMSuncal_LOSSVAR,testPRMSuncal_LOSSVAR]);
    errTable(3,74:end) = array2table([trainPRMScalib_SQRTNSE,testPRMScalib_SQRTNSE,trainPRMScalib_MOTOTNSE,testPRMScalib_MOTOTNSE,trainPRMScalib_LOSSVAR,testPRMScalib_LOSSVAR]);
    errTable(4,74:end) = array2table([trainSACSMAuncal_SQRTNSE,testSACSMAuncal_SQRTNSE,trainSACSMAuncal_MOTOTNSE,testSACSMAuncal_MOTOTNSE,trainSACSMAuncal_LOSSVAR,testSACSMAuncal_LOSSVAR]);
    errTable(5,74:end) = array2table([trainSACSMAcalib_SQRTNSE,testSACSMAcalib_SQRTNSE,trainSACSMAcalib_MOTOTNSE,testSACSMAcalib_MOTOTNSE,trainSACSMAcalib_LOSSVAR,testSACSMAcalib_LOSSVAR]);
    errTable(6,74:end) = array2table([trainIHACRESuncal_SQRTNSE,testIHACRESuncal_SQRTNSE,trainIHACRESuncal_MOTOTNSE,testIHACRESuncal_MOTOTNSE,trainIHACRESuncal_LOSSVAR,testIHACRESuncal_LOSSVAR]);
    errTable(7,74:end) = array2table([trainIHACREScalib_SQRTNSE,testIHACREScalib_SQRTNSE,trainIHACREScalib_MOTOTNSE,testIHACREScalib_MOTOTNSE,trainIHACREScalib_LOSSVAR,testIHACREScalib_LOSSVAR]);

end

%% returns water year of each date
function waterYears = getWY(dateTimes)
    nextYear = [10:12]; % months that go to next water year
    months = month(dateTimes);
    years = year(dateTimes);
    waterYears = years;
    nextYearInds = ismember(months, nextYear);
    waterYears(nextYearInds) = waterYears(nextYearInds) + 1;
end

%% computes mse, rmse, nrmse
function [rmse, nrmse, mse] = RMSE(model, observ)
    mse = nanmean((model(:)-observ(:)).^2);
    rmse = mse.^0.5;
    nrmse = rmse ./ nanstd(observ(:));
end

%% computes r^2
function r2 = R2(model, observ)
    r2 = 1 - nansum((model(:) - observ(:)).^2)/nansum((observ(:) - nanmean(observ(:))).^2);
end

%% computes mean daily bias for all and for <= and > threshold (100cfs)
function [mdfb, mdfb_lt, mdfb_gt] = MDFB(model, observ, thresh)

    if (~exist('thresh','var'))
        thresh = 100;
    end
    
    % change to column vector
    model = model(:);
    observ = observ(:);
    
    % set threshold indices
    LTind = (observ <= thresh);
    GTind = (model > thresh);
    
    % add epsilon to avoid division by zero
    epsilon = 0.001;
    observ = observ+epsilon;
    model = model+epsilon;
    
    % compute daily bias
    mdbVec = abs(model-observ) ./ observ;
    mdb_ltVec = abs(model(LTind)-observ(LTind)) ./ observ(LTind);
    mdb_gtVec = abs(model(GTind)-observ(GTind)) ./ observ(GTind);
    
    % compute mean bias
    mdfb = nanmean(mdbVec);
    mdfb_lt = nanmean(mdb_ltVec);
    mdfb_gt = nanmean(mdb_gtVec);
    
end

%% computes bias of mean flow for all in series and for <= and > threshold (100cfs)
function [mfb, mfb_lt, mfb_gt] = MFB(model, observ, thresh)

    if (~exist('thresh','var'))
        thresh = 100;
    end
    
    % change to column vector
    model = model(:);
    observ = observ(:);
    
    % set threshold indices
    LTind = (observ <= thresh);
    GTind = (model > thresh);
    
    % compute means
    modMean = nanmean(model);
    obsMean = nanmean(observ);
    modMeanLT = nanmean(model(LTind));
    obsMeanLT = nanmean(observ(LTind));
    modMeanGT = nanmean(model(GTind));
    obsMeanGT = nanmean(observ(GTind));
    
    % add epsilon to avoid division by zero
    epsilon = 0.001;
    modMean = modMean+epsilon;
    obsMean = obsMean+epsilon;
    modMeanLT = modMeanLT+epsilon;
    obsMeanLT = obsMeanLT+epsilon;
    modMeanGT = modMeanGT+epsilon;
    obsMeanGT = obsMeanGT+epsilon;
    
    % compute bias of mean
    mfb = abs(modMean-obsMean) ./ obsMean;
    mfb_lt = abs(modMeanLT-obsMeanLT) ./ obsMeanLT;
    mfb_gt = abs(modMeanGT-obsMeanGT) ./ obsMeanGT;
    
end

%% computes mean FDC bias for all and for <= and > threshold (100cfs)
function [fdcb, fdcb_lt, fdcb_gt] = FDCB(model, observ, thresh)

    if (~exist('thresh','var'))
        thresh = 100;
    end
    
    % sort observations and model
    [SortedObs, ObsInd] = sort(observ(:), 'descend');
    [SortedMod, ModInd] = sort(model(:), 'descend');
    
    % set threshold indices
    LTind = (SortedObs <= thresh);
    GTind = (SortedObs > thresh);
    
    % add epsilon to avoid division by zero
    epsilon = 0.001;
    SortedObs = SortedObs+epsilon;
    SortedMod = SortedMod+epsilon;
    
    % compute daily bias
    fdcbVec = abs(SortedMod-SortedObs) ./ SortedObs;
    fdcb_ltVec = abs(SortedMod(LTind)-SortedObs(LTind)) ./ SortedObs(LTind);
    fdcb_gtVec = abs(SortedMod(GTind)-SortedObs(GTind)) ./ SortedObs(GTind);
    
    % compute mean bias
    fdcb = nanmean(fdcbVec);
    if (isempty(fdcb_ltVec))
        fdcb_lt = NaN;
    else
        fdcb_lt = nanmean(fdcb_ltVec);
    end
    if (isempty(fdcb_gtVec))
        fdcb_gt = NaN;
    else
        fdcb_gt = nanmean(fdcb_gtVec);
    end
    
end

%% computes NSE and decomposition
function [nse, alpha_nse, beta_nse, r] = NSE(model, observ)

    % compute mean and stdev
    mu_m = nanmean(model(:));
    mu_o = nanmean(observ(:));
    std_m = nanstd(model(:));
    std_o = nanstd(observ(:));
    
    % compute nse
    nse = 1 - nansum((model(:)-observ(:)).^2) / nansum((observ(:)-mu_o).^2);

    % compute decomposition
    alpha_nse = std_m / std_o;
    beta_nse = (mu_m - mu_o) / std_o;
    r = nansum((observ(:) - mu_o) .* (model(:) - mu_m)) / (nansum((observ(:) - mu_o).^2) * nansum((model(:) - mu_m).^2))^0.5;
end

%% computes NSE and decomposition (for values < FDC percentage (60%))
function [nse, alpha_nse, beta_nse, r] = NSEL(model, observ, bot_perc)

    if (~exist('bot_perc','var'))
        bot_perc = 40;
    end
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    [Val, Ind] = sort(observ(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = 1:n;
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)

    % put model and obs in same sorted order
    observL = Val;
    modelL = model(Ind);
    
    % mask out values wbith prob < bot_perc
    observL(P < bot_perc) = NaN;
    modelL(P < bot_perc) = NaN;

    % compute mean and stdev
    mu_m = nanmean(modelL(:));
    mu_o = nanmean(observL(:));
    std_m = nanstd(modelL(:));
    std_o = nanstd(observL(:));
    
    % compute nse
    nse = 1 - nansum((modelL(:)-observL(:)).^2) / nansum((observL(:)-mu_o).^2);

    % compute decomposition
    alpha_nse = std_m / std_o;
    beta_nse = (mu_m - mu_o) / std_o;
    r = nansum((observL(:) - mu_o) .* (modelL(:) - mu_m)) / (nansum((observL(:) - mu_o).^2) * nansum((modelL(:) - mu_m).^2))^0.5;
end

%% computes NSE and decomposition (for values > FDC percentage (2%))
function [nse, alpha_nse, beta_nse, r] = NSEH(model, observ, top_perc)

    if (~exist('top_perc','var'))
        top_perc = 2;
    end
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    [Val, Ind] = sort(observ(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = 1:n;
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)

    % put model and obs in same sorted order
    observH = Val;
    modelH = model(Ind);
    
    % mask out values
    observH(P > top_perc) = NaN;
    modelH(P > top_perc) = NaN;
    
    % compute mean and stdev
    mu_m = nanmean(modelH(:));
    mu_o = nanmean(observH(:));
    std_m = nanstd(modelH(:));
    std_o = nanstd(observH(:));
    
    % compute nse
    nse = 1 - nansum((modelH(:)-observH(:)).^2) / nansum((observH(:)-mu_o).^2);

    % compute decomposition
    alpha_nse = std_m / std_o;
    beta_nse = (mu_m - mu_o) / std_o;
    r = nansum((observH(:) - mu_o) .* (modelH(:) - mu_m)) / (nansum((observH(:) - mu_o).^2) * nansum((modelH(:) - mu_m).^2))^0.5;
end

%% computes KGE and decomposition
function [kge, alpha_kge, beta_kge, r] = KGE(model, observ)
    mu_m = nanmean(model(:));
    mu_o = nanmean(observ(:));
    std_m = nanstd(model(:));
    std_o = nanstd(observ(:));
    
    alpha_kge = std_m / std_o;
    beta_kge = mu_m /  mu_o;
    r = nansum((observ(:) - mu_o) .* (model(:) - mu_m)) / (nansum((observ(:) - mu_o).^2) * nansum((model(:) - mu_m).^2))^0.5;
    kge = 1 - ((r-1)^2 + (alpha_kge-1)^2 + (beta_kge-1)^2)^0.5;
end

%% computes KGE and decomposition (for values < FDC percentage (60%))
function [kge, alpha_kge, beta_kge, r] = KGEL(model, observ, bot_perc)

    if (~exist('bot_perc','var'))
        bot_perc = 40;
    end
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    [Val, Ind] = sort(observ(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = 1:n;
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)

    % put model and obs in same sorted order
    observL = Val;
    modelL = model(Ind);
    
    % mask out values with prob < bot_perc
    observL(P < bot_perc) = NaN;
    modelL(P < bot_perc) = NaN;

    % compute mean and stdev
    mu_m = nanmean(modelL(:));
    mu_o = nanmean(observL(:));
    std_m = nanstd(modelL(:));
    std_o = nanstd(observL(:));
    
    % compute KGE
    alpha_kge = std_m / std_o;
    beta_kge = mu_m /  mu_o;
    r = nansum((observL(:) - mu_o) .* (modelL(:) - mu_m)) / (nansum((observL(:) - mu_o).^2) * nansum((modelL(:) - mu_m).^2))^0.5;
    kge = 1 - ((r-1)^2 + (alpha_kge-1)^2 + (beta_kge-1)^2)^0.5;
end

%% computes KGE and decomposition (for values > FDC percentage (2%))
function [kge, alpha_kge, beta_kge, r] = KGEH(model, observ, top_perc)

    if (~exist('top_perc','var'))
        top_perc = 2;
    end
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    [Val, Ind] = sort(observ(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = 1:n;
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)

    % put model and obs in same sorted order
    observH = Val;
    modelH = model(Ind);
    
    % mask out values wbith prob > top_perc
    observH(P > top_perc) = NaN;
    modelH(P > top_perc) = NaN;

    % compute mean and stdev
    mu_m = nanmean(modelH(:));
    mu_o = nanmean(observH(:));
    std_m = nanstd(modelH(:));
    std_o = nanstd(observH(:));
    
    % compute KGE
    alpha_kge = std_m / std_o;
    beta_kge = mu_m /  mu_o;
    r = nansum((observH(:) - mu_o) .* (modelH(:) - mu_m)) / (nansum((observH(:) - mu_o).^2) * nansum((modelH(:) - mu_m).^2))^0.5;
    kge = 1 - ((r-1)^2 + (alpha_kge-1)^2 + (beta_kge-1)^2)^0.5;
end

%% computes modified KGE and decomposition
function [kge2, beta_kge2, gamma_kge2, r] = KGE2(model, observ)
    mu_m = nanmean(model(:));
    mu_o = nanmean(observ(:));
    std_m = nanstd(model(:));
    std_o = nanstd(observ(:));
    
    beta_kge2 = mu_m /  mu_o;
    gamma_kge2 = (std_m/mu_m) / (std_o/mu_o);
    r = nansum((observ(:) - mu_o) .* (model(:) - mu_m)) / (nansum((observ(:) - mu_o).^2) * nansum((model(:) - mu_m).^2))^0.5;
    kge2 = 1 - ((r-1)^2 + (gamma_kge2-1)^2 + (beta_kge2-1)^2)^0.5;
end

%% computes top 2 % peak flow bias (FHV)
function fhv = FHV(model, observ, top_perc)
    if (~exist('top_perc','var'))
        top_perc = 2;
    end
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    [Val, Ind] = sort(observ(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = 1:n;
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)

    observH = Val;
    observH(P > top_perc) = NaN;
    fhv = 100 * nansum(model(Ind)-observH) / nansum(observH);
end

%% computes 60 % low flow bias (FLV)
function flv = FLV(model, observ, bot_perc)
    if (~exist('bot_perc','var'))
        bot_perc = 40;
    end
    minQO = min(observ(:));
    minQM = min(model(:));
    epsilon = 0.001;
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    SortedO = sort(observ(:), 'descend');
    SortedM = sort(model(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = [1:n];
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)
    
    % set observations and model to NaN for P < threshold
    observL = SortedO;
    observL(P < bot_perc) = NaN;
    modelL = SortedM;
    modelL(P < bot_perc) = NaN;
    
    % compute flv
    log_modelL = log(modelL+epsilon);
    log_minQM = log(minQM+epsilon);
    log_observL = log(observL+epsilon);
    log_minQO = log(minQO+epsilon);
    flv = -100 * (nansum(log_modelL-log_minQM) - nansum(log_observL-log_minQO)) / (nansum(log_observL-log_minQO));
    % note: flv becomes 100 when the 1st term is zero, NaN when 1st and 2nd terms are zero, Inf when just 2nd term is zero!
end

%% Bias of FDC midsegment slope (FMS)
function fms = FMS(model, observ, top_perc, bot_perc)
    if (~exist('top_perc','var'))
        top_perc = 20;
    end
    if (~exist('bot_perc','var'))
        bot_perc = 40;
    end
    epsilon = 0.001;
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    ValO = sort(observ(:), 'descend');
    ValM = sort(model(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = [1:n];
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)

    % find the discharge values for the low percent
    [val1, IndL] = min(abs(P-bot_perc));
    observLQ = ValO(IndL);
    modelLQ = ValM(IndL);

    % find the discharge values for the high percent
    [val2, IndH] = min(abs(P-top_perc));
    observHQ = ValO(IndH);
    modelHQ = ValM(IndH);
   
    fms = 100 * ((log(modelHQ+epsilon)-log(modelLQ+epsilon))-(log(observHQ+epsilon)-log(observLQ+epsilon))) / (log(observHQ+epsilon)-log(observLQ+epsilon));
end

%% computes runoff ratio
function [rr, wrr, drr] = RR(model_discharge, discharge, precip, dates, area, wetmonths, drymonths)
    if (~exist('wetmonths', 'var'))
        wetmonths = [12 1 2 3];
    end
    if (~exist('drymonths', 'var'))
        drymonths = [6 7 8 9];
    end
    months = month(dates);
    wetinds = ismember(months, wetmonths);
    dryinds = ismember(months, drymonths);
    
    % convert discharge from cfs to m^3/day and divide by basin size (m^2)
    model_discharge = model_discharge(:) .* 2446.58 ./ area;
    discharge = discharge(:) .* 2446.58 ./ area;
    
    % convert precipitation from inches/day to meters/day
    precip = precip(:) .* .0254;
    
    % add an epsilon to avoid division by zero
%     epsilon = 0.000001;
%     precip = precip + epsilon;
%     discharge = discharge + epsilon;
    
    % compute runoff ratios
    obs_rr = nansum(discharge) / nansum(precip);
    obs_wrr = nansum(discharge(wetinds)) / nansum(precip(wetinds));
    obs_drr = nansum(discharge(dryinds)) / nansum(precip(dryinds));
    mod_rr = nansum(model_discharge) / nansum(precip);
    mod_wrr = nansum(model_discharge(wetinds)) / nansum(precip(wetinds));
    mod_drr = nansum(model_discharge(dryinds)) / nansum(precip(dryinds));
    
    % compute relative metric
    rr = abs(mod_rr - obs_rr) / obs_rr;
    wrr = abs(mod_wrr - obs_wrr) / obs_wrr;
    drr = abs(mod_drr - obs_drr) / obs_drr;
end

%% computes mean and abs(mean) of residuals for values > quantile
function [fmr, fmar] = FMR(model, observ, top_perc)

%     % set flows greater than a specified quantile to NaN
%     if (~exist('quant','var'))
%         quant = 0.98;
%     end
%     
%     observHQ = quantile(observ(:),quant);
%     observ(observ <= observHQ) = NaN;
%     fmr = nanmean(model(:)-observ(:));
%     fmar = nanmean(abs(model(:)-observ(:)));

    if (~exist('top_perc','var'))
        top_perc = 2;
    end
    
    % compute FDC
    n = numel(observ(:));
    
    % Step 1: Sort (rank) average daily discharges for period of record from the largest value to the smallest value, involving a total of n values.
    [Val, Ind] = sort(observ(:), 'descend');
    % Step 2: Assign each discharge value a rank (M), starting with 1 for the largest daily discharge value.
    M = 1:n;
    % Step 3: Calculate exceedence probability (P) as follows:
    P = 100 .* (M ./ (n + 1) );

    % P = the probability that a given flow will be equaled or exceeded (% of time)
    % M = the ranked position on the listing (dimensionless)
    % n = the number of events for period of record (dimensionless)

    % put model and obs in same sorted order
    observH = Val;
    modelH = model(Ind);
    
    % mask out values wbith prob > top_perc
    observH(P > top_perc) = NaN;
    modelH(P > top_perc) = NaN;

    fmr = nanmean(modelH(:)-observH(:));
    fmar = nanmean(abs(modelH(:)-observH(:)));
end
   
%% computes mutual information normalized by observations
% Written by Laurel Larsen on 11/4/16
% I is actually the percentage of uncertainty reduction in Y (when M is [X Y])
% Modified DB2021
function I = MI(model, observ, nbins)
    if (~exist('nbins','var'))
        nbins = [11,11,11];
    end
    [~, ~, col1cat] = histcounts(model(:), nbins(2)); %Which bin the first data column is in
    [~,~,col2cat] = histcounts(observ(:),nbins(2)); %Which bin the second data column is in
    col1cat(col2cat==0)=0; %If there is an NaN for any row, assign the other column in that row to the NaN bin too
    col2cat(col1cat==0)=0; %See comment above (zero is the NaN bin)
    jointentcat = (col1cat-1)*nbins(2)+col2cat; %This classifies the joint entropy bin into a number between 1 and nbins^2. 0 is assigned to rows with misisng data.
    nbins_2 = nbins(2)^2;
    N = histcounts(jointentcat, nbins_2, 'BinLimits', [1, nbins_2]); %Number of datapoints within each joint entropy bin. Verified.
    p = N/sum(N); %Vector of probabilities
    [N1, ~, ~] = histcounts(model(:), nbins(1)); %Which bin the first data column is in
    [N2,~,~] = histcounts(observ(:),nbins(1)); %Which bin the second data column is in
    p1 = N1/sum(N1);
    p2 = N2/sum(N2);
    pgt0 = p(p>0);
    p1gt0 = p1(p1>0);
    p2gt0 = p2(p2>0);
    log2p2gt0 = log2(p2gt0);
    log2p1gt0 = log2(p1gt0);
    Hy = (-sum(p2gt0.*log2p2gt0)); %Shannon entropy of the sink variable. Used to normalize mutual informaiton in the next line. 
    I = ((-sum(p1gt0.*log2p1gt0)-sum(p2(p2>0).*log2p2gt0))+(sum(pgt0.*log2(pgt0)))*log2(nbins(1))/log2(nbins(2)))/Hy; %Mutual information, in bits. Joint entropy is scaled to the number of bins in a single dimension.
end

